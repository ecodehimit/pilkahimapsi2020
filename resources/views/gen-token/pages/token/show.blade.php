@extends('backend.master')
@section('title')
  Token
@endsection
@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Token</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="{{url('/gen-token')}}">Master</a></div>
          <div class="breadcrumb-item">Users</div>
        </div>
      </div>


      <div class="section-body">
        <h2 class="section-title">Generate</h2>

        <div class="row">
          <div class="col-12">
            <div class="card">
              <form action="" method="post">
                {{ csrf_field() }}
                <div class="card-body">
                  <input type="hidden" name="_method" value="PUT">
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-8">
                        <div class="row">
                          <div class="col-md-12">
                            <table class="table table-condensed">
                              <tr>
                                <td>Nama</td><td>{{ $data->nama }}</td>
                              </tr>
                              <tr>
                                <td>NIM</td><td>{{ $data->nrp }}</td>
                              </tr>
                              <tr>
                                <td>Token</td><td class="token">{{ $data->token }}</td>
                              </tr>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="box-footer text-center">
                    <a href="{{ url('/gen-token/token') }}" class="btn btn-warning">Back</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@stop
