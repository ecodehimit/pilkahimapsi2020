@extends('backend.master')
@section('title')
  Edit Token
@endsection
@section('content')
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Data Token
      </h1>
    </section>

    <section class="content">
      <div class="box">
        <form action="{{ url('/gen-token/token/update2/'.$data->id) }}" method="post">
          {{ csrf_field() }}
          <input type="hidden" name="_method" value="PUT">

          <div class="box-body">
            <div class="row">
              <div class="col-md-8">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="nrp">NIM</label>
                      <input type="text" class="form-control" name="nrp" id="nrp" placeholder="NIM" value="{{ $data->nrp }}" readonly required>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="box-footer text-center">
            <a href="{{ url('/gen-token/token') }}" class="btn btn-warning">Cancel</a>
            <button type="submit" name="submit" class="btn btn-success">Generate</button>
          </div>
        </form>
      </div>
    </section>
  </div>
@stop
