<!-- Left side column. contains the logo and sidebar -->
<div class="main-sidebar">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="index.html">E-VOTE 2022</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="index.html">2022</a>
    </div>
    <ul class="sidebar-menu">
      <li class="menu-header">gen-token</li>

      <li><a class="nav-link" href="{{ url('/gen-token/token') }}"><i class="far fa-file-alt"></i> <span>Token</span></a></li>

    </ul>

  </aside>
</div>
