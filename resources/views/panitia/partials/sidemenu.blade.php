<!-- Left side column. contains the logo and sidebar -->
<div class="main-sidebar">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="index.html">E-VOTE 2020</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="index.html">2020</a>
    </div>
    <ul class="sidebar-menu">
      <li class="menu-header">Starter</li>
      <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown"><i class="fas fa-star"></i> <span>Master</span></a>
        <ul class="dropdown-menu">
{{--          <li><a class="nav-link" href="{{ url('/panitia/users') }}">Users</a></li>--}}
          <li><a class="nav-link" href="{{ url('/panitia/kelas') }}">Daftar Kelas</a></li>
          <li><a class="nav-link" href="{{ url('/panitia/mahasiswas') }}">Daftar Mahasiswa</a></li>
{{--          <li><a class="nav-link" href="{{ url('/panitia/candidates') }}">Daftar Candidates</a></li>--}}
        </ul>
      </li>
{{--      <li><a class="nav-link" href="{{ url('/panitia/token') }}"><i class="far fa-file-alt"></i> <span>Token</span></a></li>--}}
{{--      <li><a class="nav-link" href="{{ url('/panitia/logs_web') }}"><i class="fas fa-unlock"></i> <span>Logs</span></a></li>--}}
{{--      <li><a class="nav-link" href="{{ url('/panitia/devices') }}"><i class="fas fa-laptop"></i> <span>Devices</span></a></li>--}}
{{--      <li><a class="nav-link" href="{{url('/panitia/suara')}}"><i class="fas fa-globe"></i> <span>Suara Masuk</span></a></li>--}}

    </ul>

  </aside>
</div>
