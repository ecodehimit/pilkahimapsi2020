@extends('backend.master')
@section('title')
  Mahasiswa
@endsection
@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Master</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active">Master</div>
          <div class="breadcrumb-item"><a href="{{ url('/panitia/mahasiswas') }}">Mahasiswa</a></div>
        </div>
      </div>
      @if (\Session::has('success'))

        <div class="alert alert-success" role="alert">
           <a href="#" class="alert-link">{!! \Session::get('success') !!}</a>
        </div>
      @endif

      @if (\Session::has('danger'))

        <div class="alert alert-danger" role="alert">
           <a href="#" class="alert-link">{!! \Session::get('danger') !!}</a>
        </div>
      @endif
      @if (\Session::has('warning'))

        <div class="alert alert-warning" role="alert">
           <a href="#" class="alert-link">{!! \Session::get('warning') !!}</a>
        </div>
      @endif

      <div class="section-body">
        <h2 class="section-title">Mahasiswa</h2>

        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4><a href="{{ url('/panitia/mahasiswa/create') }}" class="btn btn-primary">TAMBAH DATA</a> </h4>
              </div>

              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-striped" id="mahasiswa_table">
                    <thead>
                    <tr>
                      <th>Nrp</th>
                      <th>Name</th>
                      <th>Kelas</th>
                      <th>Status Pilih</th>
                      <th>Action</th>
                    </tr>
                    </thead>
{{--                    <tbody>--}}
{{--                    @foreach($data as $item)--}}

{{--                      <tr>--}}
{{--                        <td>{{ $item->nrp }}</td>--}}
{{--                        <td>{{ $item->nama }}</td>--}}
{{--                        <td>{{ $item->kelas->tingkat }} {{ $item->kelas->jenjang }} {{ $item->kelas->kelas }}</td>--}}
{{--                        <td>--}}
{{--                          @if($item->status_pilih == 'Belum')--}}
{{--                            <div class="badge badge-warning">{{ $item->status_pilih }}</div>--}}
{{--                          @else--}}
{{--                            <div class="badge badge-success">{{ $item->status_pilih }}</div>--}}
{{--                          @endif--}}

{{--                        </td>--}}
{{--                        <td>--}}
{{--                          <form action="{{ url('panitia/mahasiswa/'.$item->id) }}" method="post">--}}
{{--                            {{ csrf_field() }}--}}
{{--                            <input type="hidden" name="_method" value="DELETE">--}}
{{--                            <a href="{{ url('panitia/mahasiswa/'.$item->id.'/edit') }}" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>--}}
{{--                            <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>--}}
{{--                          </form>--}}
{{--                        </td>--}}
{{--                      </tr>--}}
{{--                    @endforeach--}}

{{--                    </tbody>--}}
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection
@section('js')
  @parent
  <script type="text/javascript">
    $(document).ready(function(){
      $('#mahasiswa_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "{{ route('mahasiswa') }}",
        },
        columns: [
          {
            data: 'nrp',
            name: 'nrp'
          },
          {
            data: 'nama',
            name: 'nama'
          },
          {
            data: 'kelas',
            name: 'kelas',
            orderable: false
          },
          {
            data: 'status_pilih',
            name: 'status_pilih'
          },
          {
            data: 'action',
            name: 'action',
            orderable: false
          },

        ]
      });
    });
  </script>
@endsection
