@extends('backend.master')
@section('title')
    Suara Masuk
@endsection
@section('content')
    <div class="card-body">

        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <h1>Data Statistik</h1>

                </div>

            </section>
            <section class="section ">
                <div class="box">
                    <div class="box-header with-border">

                    </div>
                    <div id="chartContainer" style="height: 370px; max-width: 920px; margin: 0px auto;"></div>
                    <br>
                </div>
            </section>
        </div>
    </div>

@endsection

@section('js')
    @parent
    <script>
        window.onload = function () {

            var chart = new CanvasJS.Chart("chartContainer", {
                exportEnabled: true,
                animationEnabled: true,
                title:{
                    text: "Presentasi Pemilu"
                },
                legend:{
                    cursor: "pointer",
                    itemclick: explodePie
                },

                data: [{
                    type: "pie",
                    showInLegend: true,
                    yValueFormatString: "##0.00",
                    toolTipContent: "{name}: <strong>{y}% ({jumlah} orang)</strong>",
                    indexLabel: "{name} - {y}%",
                    dataPoints: [

                    ]
                }]
            });
            chart.render();
        }

        function explodePie (e) {
            if(typeof (e.dataSeries.dataPoints[e.dataPointIndex].exploded) === "undefined" || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {
                e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;
            } else {
                e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;
            }
            e.chart.render();

        }
    </script>
@endsection
