<table id="table" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>IP Address</th>
      <th>Device Name</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
    @foreach($data as $item)
      <tr>
        <td>{{ $item->id }}</td>
        <td>{{ $item->ip_address }}</td>
        <td>{{ $item->device_name }}</td>
        <td align="center">
          @if($item->active_client == 'Aktif')
            <span class="btn btn-success">{{ $item->active_client }}</span>
          @else
            <span class="btn btn-danger">{{ $item->active_client }}</span>
          @endif
        </td>
      </tr>
    @endforeach
  </tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){
  $('#table').DataTable({
    "paging": true,
    "lengthChange": true,
    "searching": true,
    "ordering": false,
    "info": true,
    "autoWidth": false
  });
});
</script>
