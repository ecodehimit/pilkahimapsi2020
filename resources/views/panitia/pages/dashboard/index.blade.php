@extends('backend.master')
@section('title')
  Dashboard
@endsection
@section('content')

  <style media="screen">
    .highcharts-credits {display: none};
  </style>

  <div class="content-wrapper">

    <div class="main-content" id="content">
      <section class="section" >
        <div class="section-header">
          <h1>Panitia</h1>
        </div>

        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Suksesi Bem 2020</h4>
              </div>
              <div class="card-body" id="listSaksi">

                <div class="text-center">
                  <button type="button" id="pengumuman" class="btn btn-success" name="button" style="display:none">Lihat Hasil Pengumuman</button>
                </div>
              </div>

            </div>
          </div>
        </div>
      </section>
    </div>
@endsection

