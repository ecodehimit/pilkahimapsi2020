@extends('backend.master')
@section('title')
  Riwayat Login
@endsection
@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Master</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active">Master</div>
          <div class="breadcrumb-item"><a href="{{ url('/panitia/logs_web') }}">Logs Web</a></div>
        </div>
      </div>


      <div class="section-body">
        <h2 class="section-title">Logs Web</h2>

        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4>List Log Web</h4>
              </div>

              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-striped" id="table-1">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>NRP</th>
                      <th>Nama</th>
                      <th>Sebagai</th>
                      <th>Waktu</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $item)

                      <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->users->nrp }}</td>
                        <td>{{ $item->users->name }}</td>
                        <td>
                          @if($item->users->role == 0)
                            <div class="badge badge-success">admin</div>

                          @elseif($item->users->role == 1)
                            <div class="badge badge-warning">Panitia</div>
                          @else
                            <div class="badge badge-dark">saksi</div>

                          @endif

                        </td>
                        <td>{{ $item->time }}</td>

                      </tr>
                    @endforeach

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection

@section('js')
  @parent
  <script type="text/javascript">
    $(document).ready(function(){
      $('#table').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false
      });
    });
  </script>
@endsection
