@extends('backend.master')
@section('title')
  Edit Kelas
@endsection
@section('content')
  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Master</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active">Master</div>
          <div class="breadcrumb-item"><a href="{{ url('/panitia/kelas') }}">Kelas</a></div>
        </div>
      </div>

      <div class="section-body">
        <h2 class="section-title">Kelas</h2>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4>Edit Kelas</h4>
              </div>
              <form action="{{ url('/panitia/kelas/'.$data->id) }}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT">

                <div class="card-body">
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tahun</label>
                    <div class="col-sm-12 col-md-7">
                      <input type="text" class="form-control" name="tahun" id="tahun" placeholder="Tahun" value="{{ $data->tahun }}" required>
                    </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tingkat</label>
                    <div class="col-sm-12 col-md-7">
                      <input type="text" class="form-control" name="tingkat" id="tingkat" placeholder="Tingkat" value="{{ $data->tingkat }}" required>
                    </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jenjang</label>
                    <div class="col-sm-12 col-md-7">
                      <input type="text" class="form-control" name="jenjang" id="jenjang" placeholder="Jenjang" value="{{ $data->jenjang }}" required>
                    </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kelas</label>
                    <div class="col-sm-12 col-md-7">
                      <input type="text" class="form-control" name="kelas" id="kelas" placeholder="Kelas" value="{{ $data->kelas }}" required>
                    </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                    <div class="col-sm-12 col-md-7">
                      <a href="{{ url('/panitia/kelas') }}" class="btn btn-danger">Cancel</a>
                      <button type="submit" name="submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@stop
