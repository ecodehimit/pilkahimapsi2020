@extends('panitia.master')
@section('title')
  Generate Token
@endsection
@section('content')
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        New Data Token
      </h1>
    </section>

    <section class="content">
      <div class="box">
        <form action="{{ url('/backend/token') }}" method="post">
          {{ csrf_field() }}
          <div class="box-body">
            <div class="row">
              <div class="col-md-8">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="nrp">NRP</label>
                      <select name="nrp" class="form-control select2" id="nrp">
                        @foreach($mahasiswa_informatika as $item)
                          <option value="{{ $item->nrp }}">{{ $item->nrp }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="box-footer text-center">
            <a href="{{ url('/backend/token') }}" class="btn btn-warning">Cancel</a>
            <button type="submit" name="submit" class="btn btn-success">Generate</button>
          </div>
        </form>
      </div>
    </section>
  </div>
@stop
