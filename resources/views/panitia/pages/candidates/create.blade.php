@extends('backend.master')
@section('title')
Tambah Kandidat
@endsection
@section('content')
  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Master</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active">Master</div>
          <div class="breadcrumb-item"><a href="{{ url('/panitia/candidates') }}">Candidates</a></div>
        </div>
      </div>

      <div class="section-body">
        <h2 class="section-title">Candidates</h2>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4>Tambah Kandidat</h4>
              </div>
              <form action="{{ url('/panitia/candidates') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="card-body">
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Ketua Kandidat</label>
                    <div class="col-sm-12 col-md-7">
                      <select class="form-control selectric" id="ketua" name="ketua">
                        <option value="" disabled selected>Pilih Calon Kahima</option>
                        @foreach($mahasiswa as $item)
                          <option value="{{ $item->nama }}">{{ $item->nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Wakil Kandidat</label>
                    <div class="col-sm-12 col-md-7">
                      <select class="form-control selectric" id="wakil" name="wakil">
                        <option value="" disabled selected>Pilih Calon Wakahima</option>
                        @foreach($mahasiswa as $item)
                          <option value="{{ $item->nama }}">{{ $item->nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">No Urut</label>
                    <div class="col-sm-12 col-md-7">
                      <input type="text" class="form-control" name="no_urut" id="no_urut" placeholder="No Urut" required>
                    </div>
                  </div>

                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Upload Image</label>
                    <div class="col-sm-12 col-md-7">
                      <input type="file" name="image" id="image-source" onchange="previewImage();"/>
                      <img id="image-preview" alt="imag e preview"/>
                    </div>
                  </div>


                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Visi</label>
                    <div class="col-sm-12 col-md-7">
                      <textarea class="summernote-simple" name="visi" style="width: 100%;height: 100px" placeholder="Visi"></textarea>
                    </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Misi</label>
                    <div class="col-sm-12 col-md-7">
                      <textarea class="summernote-simple" name="misi" style="width: 100%;height: 100px" placeholder="Misi"></textarea>
                    </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                    <div class="col-sm-12 col-md-7">
                      <a href="{{ url('/panitia/candidates') }}" class="btn btn-danger">Cancel</a>
                      <button type="submit" name="submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <script type="text/javascript">

    function previewImage() {
      document.getElementById("image-preview").style.display = "block";
      var oFReader = new FileReader();
      oFReader.readAsDataURL(document.getElementById("image-source").files[0]);

      oFReader.onload = function(oFREvent) {
        document.getElementById("image-preview").src = oFREvent.target.result;
      };
    };
    $('#ketua').on('change', function(){
      var ketua = $(this).val();
      $.ajax({
        type : 'get',
        url : "{{url('/panitia/candidates/show')}}",
        data : {'nama' : ketua},
        success : function(data){
          $('#wakil').html(data);
        }
      });
    });
  </script>
@stop
