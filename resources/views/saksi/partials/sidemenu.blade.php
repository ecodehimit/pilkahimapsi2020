<!-- Left side column. contains the logo and sidebar -->
<div class="main-sidebar">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="index.html">E-VOTE 2021</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="index.html">2021</a>
    </div>
    <ul class="sidebar-menu">
      <li class="menu-header">Dashboard</li>
      <li class="nav-item dropdown active">
        <a href="{{ url('/saksi') }}"  class="nav-link"><i class="fas fa-newspaper"></i><span>Pengumuman</span></a>
      </li>
      <li class="menu-header">Starter</li>
      <li><a class="nav-link" href="{{url('/saksi/suara')}}"><i class="fas fa-globe"></i> <span>Suara Masuk</span></a></li>

    </ul>

  </aside>
</div>
