@extends('backend.master')
@section('title')
    Pengumuman Hasil
@endsection

@section('content')
    <style media="screen">
        .highcharts-credits {display: none};
    </style>

    <div class="content-wrapper">

        <div class="main-content" id="content">
        <section class="section" >
            <div class="section-header">
                <h1>Pengumuman</h1>
            </div>

            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Hasil Pemungutan Suara</h4>
                        </div>
                        <div class="card-body" id="listSaksi">
                            <div class="table-responsive">
                                <table class="table table-bordered table-md">
                                    <tr>
                                        <th>Jabatan</th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <td>Ketua Pelaksana PILKAHIMAPSI</td>
                                        <td><span id="ketua_pelaksana"></span></td>
                                    </tr>
                                    <tr>
                                        <td>Panwas 1</td>
                                        <td><span id="panwas1"></span></td>
                                    </tr>
                                    <tr>
                                        <td>Panwas 2</td>
                                        <td><span id="panwas2"></span></td>
                                    </tr>
                                    <tr>
                                        <td>Saksi Paslon</td>
                                        <td><span id="saksi_paslon"></span></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="text-center">
                                <button type="button" id="pengumuman" class="btn btn-success" name="button" style="display:none">Lihat Hasil Pengumuman</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
@section('js')
    @parent

    <script type="text/javascript">

        var cek_saksi = setInterval(function () {
            $.ajax({
                url : "{{url('/saksi/dashboard/update')}}",
                type : 'put',
                data : { _token : '{{csrf_token()}}'},
                success : function(data){
                    $('#listSaksi').html(data);
                }
            });
        }, 3000);

    </script>
@endsection