<section class="section" >
  <div class="section-header">
    <h1>Pengumuman</h1>
  </div>

  <div class="row">
    <div class="col-12 col-md-6 col-lg-12">
      <div class="card">
        <div class="card-header">
          <h4>Data Statistik</h4>
        </div>
        <div class="card-body">

          <div class="box-body">
            <div id="chart-bar"></div>
          </div>
          <br><br>
          <button type="button" name="button" class="btn btn-success"  id="hasil">Hasil Pengumuman</button>

        </div>


      </div>
    </div>
  </div>
</section>

<div class="modal fade" role="dialog" id="calon1">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width: 500px">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <h4 class="modal-title">Hasil PILKAHIMAPSI 2021</h4>
        <h1 style="color: orange; margin-bottom:10px">{{$calon1->ketua}}</h1>
        <h1 style="color: orange; margin-bottom:10px">{{$calon1->wakil}}</h1>
          <p>sebagai</p>
          <h5>Ketua dan Wakil Ketua HIMAPSI UB Periode 2022</h5>
        {{-- <img src="{{asset('img/01.png')}}" alt="" width="400px"> --}}
        <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-check" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z"/>
        </svg>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" role="dialog" id="calon2">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <h4 class="modal-title">Hasil PILKAHIMAPSI 2021</h4>
        <h1 style="color: orange; margin-bottom:10px">{{$calon2->ketua}}</h1>
        <h1 style="color: orange; margin-bottom:10px">{{$calon2->wakil}}</h1>
          <p>sebagai</p>
          <h5>Ketua dan Wakil Ketua HIMAPSI UB Periode 2022</h5>
        <img src="{{asset('img/01.png')}}" alt="" width="400px">
      </div>
    </div>
  </div>
</div>

<div class="modal fade" role="dialog" id="seri">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hasil PILKAHIMAPSI 2021</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center"><br><br><br>
        <h2>SERI</h2><br><br><br>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('#chart-bar').highcharts({
    chart : {
      type : 'column'
    },
    title : {
      text : 'Hasil Voting'
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Total Vote'
      }
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          format: '{point.y:.0f} Suara'
        }
      }
    },

    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f} Suara</b><br/>'
    },

    xAxis : {
      categories : ['{{$calon1->ketua}}', '{{$calon2->ketua}}']
    },

    series: [{
      name: 'Perolehan suara',
      colorByPoint: true,
      data: [
        0, 0,
      ],
    }],
  });

  $('#hasil').click(function(){
    $(this).css("display", "none");
    var rand1, rand2, max;
    var total1 = {{$total1}}, total2 = {{$total2}};
    var calon1 = {{$total1}}; calon2 = {{$total2}};
    var hasil1 = 0, hasil2 = 0;
    var chart = $('#chart-bar').highcharts();
    var newData;

    var interval = setInterval(function () {
      if(total1 >= 0 && total2 >= 0){
        rand1 = Math.floor((Math.random() * 3) + 1);
        rand2 = Math.floor((Math.random() * 3) + 1);

        if(total1 < rand1){
          hasil1 += total1;
        }else{
          hasil1 += rand1;
        }

        if(total2 < rand2){
          hasil2 += total2;
        }else{
          hasil2 += rand2;
        }

        newData = [hasil1, hasil2];

        chart.series[0].setData(newData);

        if(total1 < rand1){
          total1 -= total1;
        }else{
          total1 -= rand1;
        }

        if(total2 < rand2){
          total2 -= total2;
        }else{
          total2 -= rand2;
        }

      }
      // console.log(total1, total2, total3);
      if (total1 == 0 && total2 == 0) {
        clearInterval(interval);
        if(calon1 > calon2){
          $('#calon1').modal('show');
        }else if(calon2 > calon1){
          $('#calon2').modal('show');
        }else{
          $('#seri').modal('show');
        }

      }
    }, 1000);
  });


</script>