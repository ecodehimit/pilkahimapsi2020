<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{asset('img/evote_logo.png')}}">
    <title>E-Vote PILKAHIMAPSI UB 2021</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">


    <!-- template-->
    <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('js/semantic.js')}}"></script>


    <!-- Template CSS -->

    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link rel="stylesheet" href="{{url('css/material-kit.css')}}">
    <!-- Documentation extras -->

    <style>
        .logo-landing{
            height: 75px;
            width: 75px;
        }
        .card-content{
            font-weight: bold;
            font-size: 12px;
            margin-top:10px
        }
    </style>

</head>

<body class="login-page" style="background-color:#6E0081;">
    <div class="header-landing">
        <nav class="navbar-landing" style="height: 46px; background-color: #440450"></nav>
        <div class="d-flex flex-column justify-content-between flex-md-row align-items-center">
            <div class="ml-md-3">
                <img class="logo-landing" src=img/logo_ub.png>
                <img class="logo-landing" src=img/logo_himapsi.png>
            </div>
            <div class="mr-md-3 content-landing text-center">
                <h6 style="color: white;">Copyright by alumni E-Code HIMIT’22</h6>
                <div class="digital-clock">
                    <iframe src="https://free.timeanddate.com/clock/i82ovc6r/n631/fn2/fs28/fcf5b910/tct/pct/ftb/th1" frameborder="0" width="133" height="41" allowtransparency="true"></iframe>
                </div>
            </div>
        </div>
    </div>

{{--        <div class="card mx-auto">--}}
{{--            <div class="card-header">--}}
{{--                Featured--}}
{{--            </div>--}}
{{--            <div class="card-body">--}}
{{--                <form method="POST" action="{{ url('/') }}">--}}
{{--                <input type="hidden" id="api_key" name="api_key" value="468183fe-0d97-4398-8674-22673be0597c">--}}
{{--                <!-- <input type="hidden" id="device" name="device" value="a"> -->--}}
{{--                    {{ csrf_field() }}--}}
{{--                    <div class="form-group">--}}
{{--                        <label for="exampleInputEmail1">NRP</label>--}}
{{--                        <input type="text" id="nrp" name="nrp" class="form-control" aria-describedby="emailHelp">--}}
{{--                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <label for="exampleInputPassword1">TOKEN</label>--}}
{{--                        <input type="password" class="form-control" id="token" name="token">--}}
{{--                    </div>--}}
{{--                    <div class="form-group form-check">--}}
{{--                        <input type="checkbox" class="form-check-input" id="exampleCheck1">--}}
{{--                        <label class="form-check-label" for="exampleCheck1">Check me out</label>--}}
{{--                    </div>--}}
{{--                    <button type="submit" class="btn btn-primary">Submit</button>--}}
{{--                </form>--}}
{{--            </div>--}}

{{--        </div>--}}

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6 ml-auto mr-auto" style="margin-top: 50px">
                <div class="card card-signup">
                    <form class="form" method="POST" action="{{ url('/vote') }}">
                        {{ csrf_field() }}
                        @csrf
                        <div class="card-header card-header-primary text-center">
                            <h4 class="card-title">Log in</h4>
                            <h5><b>E-VOTE PILKAHIMAPSI UB 2022</b></h5>
                        </div>
                        @if (\Session::has('success'))
                            <div class="card-header card-header-success text-center">
                                {!! \Session::get('success') !!}
                            </div>
                        @endif
                        @if (\Session::has('danger'))
                            <div class="card-header card-header-danger text-center">
                                {!! \Session::get('danger') !!}
                            </div>
                        @endif
                        @if (\Session::has('warning'))
                            <div class="card-header card-header-warning text-center">
                                {!! \Session::get('warning') !!}
                            </div>
                        @endif

                        <div class="card-body">
                            <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">face</i>
                                    </span>
                                <input type="text" id="nrp" name="nrp" class="form-control" placeholder="NIM" value="{{ old('nrp') }}">
                            </div>
                            <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">lock_outline</i>
                                    </span>
                                <input type="password" id="token" name="token" class="form-control" placeholder="TOKEN">
                            </div>

                        </div>
                        <div class="footer text-center">
                            <button type="submit" class="btn btn-primary btn-link btn-wd btn-lg">Vote Now!</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
{{--    <div class="footer-landing align-self-center text-center">--}}
{{--        <p class="card-content" style="color: white; letter-spacing: 2px">--}}
{{--            Narahubung RSVP: Meila (0823-3838-2734/meyla.12), Athaya (0858-4827-7628/athayaaf), <br>--}}
{{--            Dyah (0813-3624-3340/keynn1), Zahra (0858-4112-0523/zaracvwfmccann), Naura(0819-9898-1788/raranabilaa)--}}
{{--        </p>--}}
{{--    </div>--}}

</body>

</html>
