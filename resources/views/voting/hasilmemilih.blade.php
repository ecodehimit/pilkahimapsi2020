<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{asset('img/evote_logo.png')}}">
    <title>Telah Memilih</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">


    <!-- template-->
    <link rel="stylesheet" href="{{asset('css/')}}">
    <link rel="stylesheet" href="{{asset('css/semantic.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/icon.min.css')}}">
    <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('js/semantic.js')}}"></script>


    <!-- Template CSS -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/components.css')}}">
    <style>
        body{

            position: relative;
            background: #22262b;
            overflow: hidden;


        }
        body:before {
            content: ' ';
            display: block;
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            z-index: -1;
            background-image: url({{asset('img/bgfix2.png')}});
            background-repeat: no-repeat;
            background-position: 50% 0;
            background-size: cover;
        }
    </style>
<body>

<div class="container" style="height: 100vh">
    @if (\Session::has('success'))
        <div class="row" style="height: 600px">
        <div class="col align-self-center text-center" style="margin-top: 100px">
            <center>
                <div class="card " style="padding: 20px">
                    <div class="card-body">
                        <h1 class="card-title text-success font-weight-bold">Selamat Anda telah memilih</h1>
                           <h5 class="card-text font-weight-bold">Terima kasih telah menggunakan hak pilih Anda!</h5>
                    </div>
                </div>
            </center>
        </div>
</div>
@endif
</div>
</body>
<script>
    setTimeout(function() {
        window.location = "{{url('/vote')}}";
    }, 30000);
</script>
</html>
