<!DOCTYPE html>
<html lang="en">
<head>
   {{-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> --}}
   <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
   <script>
           console.log('asd')
           function validasi() {
               $.ajax(
                   {
                       url: '{{url('validasi/'.$data->nrp)}}',
                       type: 'get',
                       data: {},
                       success: function (data) {
                           if (data == true) {
                               location.reload();
                           }
                           console.log(data)
                       }
                   }
               );
           }
           var waktu = window.setInterval(function () {
                validasi()
           }, 2000);
   </script>

   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- CSRF Token -->
   <meta name="csrf-token" content="{{ csrf_token() }}">
   <link rel="shortcut icon" href="{{asset('img/evote_logo.png')}}">
   <title>E-Vote PILKAHIMAPSI UB 2021</title>

   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

   <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

   <!-- template-->
   <link rel="stylesheet" href="{{asset('css/')}}">
   <link rel="stylesheet" href="{{asset('css/semantic.css')}}">
   <link rel="stylesheet" href="{{asset('css/main.css')}}">
   <link rel="stylesheet" href="{{asset('css/icon.min.css')}}">
   <script src="{{asset('js/semantic.js')}}"></script>

   <!-- Template CSS -->
   <link rel="stylesheet" href="{{asset('css/style.css')}}">
   <link rel="stylesheet" href="{{asset('css/components.css')}}">


</head>
<style>
    body{
        position: relative;
        background: #22262b;
    }
    body:before {
        content: ' ';
        display: block;
        position: fixed;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        z-index: -1;
        background-image: url({{asset('img/bgfix.png')}});
        background-repeat: no-repeat;
        background-position: 50% 0;
        background-size: cover;
    }
</style>
<body>
<div class="container text-white" style="height: 100vh; padding: 0 25px 25px">
    <h5 style="padding-top: 20px; padding-left: 20px">{{$data->nrp}}</h5>
    <h5 style="padding-left: 20px">Hai, {{$mahasiswa->nama}}</h5>
    <br>
        <center>
            <div class="row">
                <div class="col-md-6 align-self-center" styl>
                    <form id="pilih" action="{{url('/pilih')}}" method="post">
                        {{ csrf_field() }}
                        <div class="card text-dark" style="width: 500px;height: 1050px">
                            <div class="card-body align-self-center" style="width: 400px">
                                <h6>PASLON 1</h6>

                                <img class="card-img-top" src="{{asset('img/calon1.jpg')}}" alt="Card image cap">

                                <div class="row">
                                    <div class="col-md-6 align-self-center">
                                        <p class="card-title">Ketua Himpunan</p><br>
                                    </div>
                                    <div class="col-md-6 align-self-center">
                                        <p class="card-title">Wakil Ketua Himpunan</p><br>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 align-self-center">
                                        <h6 class="card-title">{{$kandidat1->ketua}}</h6><br>
                                    </div>
                                    <div class="col-md-6 align-self-center">
                                        <h6 class="card-title">{{$kandidat1->wakil}}</h6><br>
                                    </div>
                                </div>

                                <input id="no_urut" type="hidden" name="no_urut" value="">
                                <input type="hidden" name="nrp" value="{{$data->nrp}}">
                                <input type="hidden" name="token" value="{{$data->token}}">
                                <button type="button" class="btn btn-primary float-center" id="btn-yes">VOTE</button>
                            </div>

                            <div class="text-left" style="padding : 0 25px 25px">
                                <h2 class="title">
                                    Visi
                                </h2>
                                <a class="title">{{$kandidat1->visi}}</a>
                                <br><br>
                                <h2 class="title">
                                    Misi
                                </h2>
                                <a class="title">{!!$kandidat1->misi!!}</a>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-md-6 align-self-center">
                    <form id="pilih" action="{{url('/pilih')}}" method="post">
                        {{ csrf_field() }}
                        <div class="card text-dark" style="width: 500px;height: 1050px">
                            <div class="card-body align-self-center" style="width: 400px">
                                <h6>PASLON 2</h6>

                                <img class="card-img-top" src="{{asset('img/calon2.jpg')}}" alt="Card image cap">

                                <div class="row">
                                    <div class="col-md-6 align-self-center">
                                        <p class="card-title">Ketua Himpunan</p><br>
                                    </div>
                                    <div class="col-md-6 align-self-center">
                                        <p class="card-title">Wakil Ketua Himpunan</p><br>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 align-self-center">
                                        <h6 class="card-title">{{$kandidat2->ketua}}</h6><br>
                                    </div>
                                    <div class="col-md-6 align-self-center">
                                        <h6 class="card-title">{{$kandidat2->wakil}}</h6><br>
                                    </div>
                                </div>

                                <input id="no_urut" type="hidden" name="no_urut" value="{{$kandidat2->no_urut}}">
                                <input type="hidden" name="nrp" value="{{$data->nrp}}">
                                <input type="hidden" name="token" value="{{$data->token}}">
                                <button type="button" class="btn btn-primary float-center" id="btn-not">VOTE</button>
                            </div>
                            <div class="text-left" style="padding : 0 25px 25px">
                                <h2 class="title">
                                    Visi
                                </h2>
                                <a class="title">{{$kandidat2->visi}}</a>
                                <br><br>
                                <h2 class="title">
                                    Misi
                                </h2>
                                <a class="title">{!!$kandidat2->misi!!}</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </center>
    <br><br>
</div>

<script>

    $(document).ready(function() {
        swal('Anda memiliki waktu 10 menit untuk memilih', {
                icon: "warning",
            });
        setTimeout(function() {
            $('#no_urut').val('-1'); //suara tidak sah
            $('form').submit();
        }, 600000); //waktu dalam vote

        $(document).on('click', '#btn-yes', function(e) {
            $('#no_urut').val(1); //setuju
            var form = $(this).parents('form');
            swal({
                title: "Apakah anda yakin?",
                icon: "warning",
                buttons:[
                    'Tidak, batalkan!',
                    'Ya, Saya yakin!'
                ],
            }).then(function(isConfirm){
                if(isConfirm){
                    setTimeout(function() {
                        form.submit()
                    }, 1000);
                    clearInterval(waktu);
                    swal('Berhasil memilih', {
                        icon: 'success',
                    });
                }
                else {
                    swal('Batal memilih');
                }
            });
        });

        $(document).on('click', '#btn-not', function(e){
            $('#no_urut').val(2); //tidak setuju
            var form = $(this).parents('form');
            swal({
                title: "Apakah anda yakins?",
                icon: "warning",
                buttons:[
                    'Tidak, batalkan!',
                    'Ya, Saya yakin!'
                ],
            }).then(function(isConfirm){
                if(isConfirm){
                    setTimeout(function() {
                        form.submit()
                    }, 1000);
                    clearInterval(waktu);
                    swal('anda telah memilih paslon', {
                        icon: 'success',
                    });
                }
                else {
                    swal('Batal memilih');
                }
                });
            });
        });
</script>

</body>
</html>
