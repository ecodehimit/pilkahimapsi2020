@extends('index')
@section('title')
	E-Vote
@endsection

@section('content')
	<div id="app">
		<section class="section" style="padding:0">
			<div class="d-flex flex-wrap align-items-stretch">
				<div class="col-lg-4 col-md-6 col-12 order-lg-1 min-vh-100 order-2 bg-white">
					<div class="p-4 m-3">
						<img src="{{asset('img/evote_logo.png')}}" alt="logo" width="80" class="shadow-light rounded-circle mb-5 mt-2">
						<h4 class="text-dark font-weight-normal">E-Vote <span class="font-weight-bold">2021</span></h4>
						<p class="text-muted">PILKAHIMAPSI UB 2021</p>
						<form method="POST" action="{{ url('auth/login') }}" class="needs-validation" novalidate="">
							{{csrf_field()}}
							<div class="form-group">
								<label for="email">NIM</label>
								<input id="email" type="text" class="form-control" name="nrp" tabindex="1" required autofocus value="{{ old('nrp') }}">
								<div class="invalid-feedback">
									Masukkan NIM anda
								</div>
							</div>

							<div class="form-group">
								<div class="d-block">
									<label for="password" class="control-label">Password</label>
								</div>
								<input id="password" type="password" class="form-control" name="password" tabindex="2" required>

								<div class="invalid-feedback">
									Masukkan password anda
								</div>
							</div>
							<!--
							<div class="form-group">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" name="remember" class="custom-control-input" tabindex="3" id="remember-me">
									<label class="custom-control-label" for="remember-me">Remember Me</label>
								</div>
							</div>
							-->
							<div class="form-group text-right">

								<button type="submit" class="btn btn-primary btn-lg btn-icon icon-right" tabindex="4">
									Login
								</button>
							</div>
						</form>
						<div class="text-center mt-5 text-small">
							Copyright &copy; E-CODE 2019. Made with 💙 in Surabaya
							<div class="mt-2">
								<a href="#">Privacy Policy</a>
								<div class="bullet"></div>
								<a href="#">Terms of Service</a>
							</div>
						</div>

					</div>
				</div>
				<div class="col-lg-8 col-12 order-lg-2 order-1 min-vh-100 background-walk-y position-relative overlay-gradient-bottom" data-background="{{asset('img/white.jpg')}}">
				</div>
			</div>
		</section>
	</div>

	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
	<script src="{{asset('js/stisla.js')}}"></script>


	<!-- JS Libraies -->

	<!-- Template JS File -->
	<script src="{{asset('js/scripts.js')}}"></script>
	<script src="{{asset('js/custom.js')}}"></script>
@endsection