<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="shortcut icon" href="{{asset('img/evote_logo.png')}}">
  <title>@yield('title') | E-Vote</title>
  @section('css')
    <link href="https://fonts.googleapis.com/css?family=Hind:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ url('vendor/highcharts-editor/highcharts-editor.min.css') }}">
    <link rel="stylesheet" href="{{asset('css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatable.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/')}}">
    <link rel="stylesheet" href="{{asset('css/icon.min.css')}}">
    <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>

{{--    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>--}}
{{--    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>--}}
{{--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>--}}


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/components.css')}}">

@show

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<style>
  #image-preview{
    display:none;
    width : 250px;
    height : 300px;
  }
</style>
<body>
<div id="app">
  <div class="main-wrapper">

    @include('backend.partials.header')
    <div class="main-sidebar">
      @if(auth()->user()->role_web == 'admin')

        @include('backend.partials.sidemenu')
      @elseif(auth()->user()->role_web == 'saksi')
        @include('saksi.partials.sidemenu')
      @elseif(auth()->user()->role_web == 'gen-token')
        @include('gen-token.partials.sidemenu')
      @elseif(auth()->user()->role_web == 'panitia')
        @include('panitia.partials.sidemenu')
      @endif
    </div>

    <div class="wrapper">
      @yield('content')
    </div>

    @include('backend.partials.footer')

    @section('js')
      <script type="text/javascript" src="{{ url('js/jquery.form.js') }}"></script>
      <script type="text/javascript" src="{{ url('js/jquery.MetaData.js') }}"></script>
      <script type="text/javascript" src="{{ url('js/jquery.MultiFile.js') }}"></script>
      <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
      <script>
        $.widget.bridge('uibutton', $.ui.button);
      </script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
      <!-- Select2 -->

      <!-- jQuery Knob Chart -->
      <!-- AdminLTE App -->
      <script type="text/javascript" src="{{ url('vendor/highcharts/code/highcharts.src.js') }}"></script>
{{--      <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>--}}
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
      <script src="{{asset('js/stisla.js')}}"></script>


      <!-- JS Libraies -->
      <script src="{{asset('js/canvasjs.min.js')}}"></script>

      <!-- Template JS File -->
      <script src="{{asset('js/select2.full.min.js')}}"></script>
      <script src="{{asset('js/dataTables.select.min.js')}}"></script>
      <script src="{{asset('js/datatable.min.js')}}"></script>
      <script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>
      <script src="{{asset('js/page/modules-datatables.js')}}"></script>

      <script src="{{asset('js/scripts.js')}}"></script>
      <script src="{{asset('js/custom.js')}}"></script>
      <script type="text/javascript">
        $(document).ready(function(){
          $(".select2").select2();

          $('.upload').click(function(){
            $('.user-photo input[type="file"]').click();
            return false;
          });

          function readURL(input) {
            if (input.files && input.files[0]) {
              var reader = new FileReader();

              reader.onload = function (e) {
                $('#preview-image').attr('src', e.target.result);
                $('#photo').val(e.target.result);
              }

              reader.readAsDataURL(input.files[0]);
            }
          }

          $("#image").change(function(){
            if (window.File && window.FileReader && window.FileList && window.Blob) {
              //get the file size and file type from file input field
              var fsize = $(this)[0].files[0].size;

              if(fsize > 1048576) { //do something if file size more than 1 MB (1048576)
                alert("Ukuran file terlalu besar");
                $(this).val('');
              }else {
                $("#preview-image").css('opacity','1');
                readURL(this);
              }
            }else{
              alert("Silahkan upgrade browser untuk untuk mendapatkan fitur validasi file max size");
              $("#preview-image").css('opacity','1');
              readURL(this);
            }
          });
        });
      </script>
    @show

    @if (session('message'))
      <script type="text/javascript">
        $(document).ready(function() {

          $.notify({
            title: "<strong>{{ session('message')['title'] }}</strong> ",
            message: "{{ session('message')['msg'] }}",
          },{
            type: "{{ session('message')['type'] }}",
            placement: {
              from: "top",
              align: "right",
            },
          });
        });
      </script>
    @endif
    @yield('script')
  </div>
</div>

</body>
</html>
