<!-- Left side column. contains the logo and sidebar -->
<div class="main-sidebar">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="{{url('/backend')}}">E-VOTE 2022</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="{{url('/backend')}}">2022</a>
    </div>
    <ul class="sidebar-menu">
      <li class="menu-header">Dashboard</li>
      <li class="nav-item dropdown active">
        <a href="{{url('/backend')}}" class="nav-link"><i class="fas fa-newspaper"></i><span>Pengumuman</span></a>
      </li>
      <li class="menu-header">Starter</li>

      <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown"><i class="fas fa-star"></i> <span>Master</span></a>
        <ul class="dropdown-menu">
          <li><a class="nav-link" href="{{ url('/backend/users') }}">Users</a></li>
          <li><a class="nav-link" href="{{ url('/backend/kelas') }}">Daftar Kelas</a></li>
          <li><a class="nav-link" href="{{ url('/backend/mahasiswas') }}">Daftar Mahasiswa</a></li>
          <li><a class="nav-link" href="{{ url('/backend/candidates') }}">Daftar Kandidat</a></li>
        </ul>
      </li>
      <li><a class="nav-link" href="{{ url('/backend/token') }}"><i class="far fa-file-alt"></i> <span>Token</span></a></li>
      <li><a class="nav-link" href="{{ url('/backend/logs_web') }}"><i class="fas fa-unlock"></i> <span>Logs</span></a></li>
      <li><a class="nav-link" href="{{ url('/backend/devices') }}"><i class="fas fa-laptop"></i> <span>Devices</span></a></li>
      <li><a class="nav-link" href="{{url('/backend/suara')}}"><i class="fas fa-globe"></i> <span>Suara Masuk</span></a></li>

    </ul>

  </aside>
</div>
