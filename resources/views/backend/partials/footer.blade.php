<footer class="main-footer">
  <div class="footer-left">
    E-CODE &copy; 2021 <div class="bullet"></div> Made with 💙 in Surabaya
  </div>
  <div class="footer-right">
    1.0.0
  </div>
</footer>