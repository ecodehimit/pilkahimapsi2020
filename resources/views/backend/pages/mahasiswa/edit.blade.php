@extends('backend.master')
@section('title')
  Edit Mahasiswa
@endsection
@section('content')
  <<!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Master</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active">Master</div>
          <div class="breadcrumb-item"><a href="{{ url('/backend/mahasiswas') }}">Mahasiswa</a></div>
        </div>
      </div>

      <div class="section-body">
        <h2 class="section-title">Mahasiswa</h2>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4>Edit Mahasiswa</h4>
              </div>
              <form action="{{ url('/backend/mahasiswa/'.$data->id) }}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT">

                <div class="card-body">
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">NIM</label>
                    <div class="col-sm-12 col-md-7">
                      <input type="text" class="form-control" name="nrp" id="nrp" placeholder="NIM" value="{{ $data->nrp }}" required>
                    </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
                    <div class="col-sm-12 col-md-7">
                      <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="{{ $data->nama }}" required>
                    </div>
                  </div>

                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kelas/Tahun Angkatan</label>
                    <div class="col-sm-12 col-md-7">
                      <select name="kelas_id" id="kelas_id" class="form-control selectric">
                        @foreach($kelas as $item)
                          @if($data->kelas_id == $item->id)
                            <option value="{{ $item->id }}" selected>{{ $item->tahun }}</option>
                          @else
                            <option value="{{ $item->id }}">{{ $item->tahun }}</option>
                          @endif
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Status Pilih</label>
                    <div class="col-sm-12 col-md-7">
                      <select name="status_pilih" id="status_pilih" class="form-control selectric">
                        @if($data->status_pilih == 'Belum')
                          <option value="Belum">Belum</option>
                        @else
                          <option value="Belum">Belum</option>
                        @endif
                        @if($data->status_pilih == 'Sudah')
                          <option value="Sudah">Sudah</option>
                        @else
                          <option value="Sudah">Sudah</option>
                        @endif
                      </select>
                    </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                    <div class="col-sm-12 col-md-7">
                      <a href="{{ url('/backend/mahasiswa') }}" class="btn btn-danger">Cancel</a>
                      <button type="submit" name="submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@stop
