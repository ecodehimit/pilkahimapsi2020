@extends('backend.master')
@section('title')
  Import Data Mahasiswa
@endsection
@section('content')
  <<!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Master</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active">Master</div>
          <div class="breadcrumb-item"><a href="{{ url('/backend/mahasiswas') }}">Mahasiswa</a></div>
        </div>
      </div>

      <div class="section-body">
        <h2 class="section-title">Mahasiswa</h2>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4>Import Data Mahasiswa</h4>
              </div>
              <form action="{{ url('/backend/mahasiswas/import') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="card-body">
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Pilih file excel</label>
                    <div class="col-sm-12 col-md-7">
                      <input type="file" name="excel" id="excel" class="form-control" placeholder="File Excel" required>
                    </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                    <div class="col-sm-12 col-md-7">
                      <a href="{{ url('/backend/mahasiswa') }}" class="btn btn-danger">Cancel</a>
                      <button type="submit" name="submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@stop
