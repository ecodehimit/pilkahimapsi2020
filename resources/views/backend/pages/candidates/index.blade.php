@extends('backend.master')
@section('title')
Kandidat
@endsection
@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Master</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active">Master</div>
          <div class="breadcrumb-item"><a href="{{ url('/backend/candidates') }}">Candidates</a></div>
        </div>
      </div>


      <div class="section-body">
        <h2 class="section-title">Candidates</h2>

        <div class="row">
          <div class="col-12">
            <div class="card">
              {{-- <div class="card-header">
                <h4><a href="{{ url('/backend/candidates/create') }}" class="btn btn-primary">TAMBAH DATA</a> </h4>
              </div> --}}

              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-striped" id="table-1">
                    <thead>
                    <tr>
                      <th>Jenis Suara</th>
                      <th>Kode</th>
                      {{-- <th>Action</th> --}}
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $item)

                      <tr>
                        <td>{{ $item->ketua }}</td>
                        <td>{{ $item->no_urut }}</td>
                        {{-- <td>
                          <form action="{{ url('backend/candidates/'.$item->id) }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="DELETE">
                            <a href="{{ url('backend/candidates/'.$item->id.'/edit') }}" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>
                            <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>
                          </form>
                        </td> --}}
                      </tr>
                    @endforeach

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection

