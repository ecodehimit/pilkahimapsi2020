@extends('backend.master')
@section('title')
  Edit Kandidat
@endsection
@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Master</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active">Master</div>
          <div class="breadcrumb-item"><a href="{{ url('/backend/candidates') }}">Candidates</a></div>
        </div>
      </div>

      <div class="section-body">
        <h2 class="section-title">Candidates</h2>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4>Edit Kandidat</h4>
              </div>
              <form action="{{ url('/backend/candidates/'.$data->id) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT">

                <div class="card-body">
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Ketua Kandidat</label>
                    <div class="col-sm-12 col-md-7">
                      <select class="form-control selectric" id="ketua" name="ketua">
                        <option value="{{ $data->ketua}}" disabled selected>{{ $data->ketua}}</option>
                        @foreach($mahasiswa as $item)
                          @if($data->ketua == $item->nama)
                            <option value="{{ $item->nama }}">{{ $item->nama }}</option>
                          @else
                            <option value="{{ $item->nama }}">{{ $item->nama }}</option>
                          @endif
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Wakil Kandidat</label>
                    <div class="col-sm-12 col-md-7">
                      <select class="form-control selectric" id="wakil" name="wakil">
                        <option value="{{ $data->wakil}}" disabled selected>{{ $data->wakil}}</option>
                        @foreach($mahasiswa as $item)
                          @if($data->wakil == $item->nama)
                            <option value="{{ $item->nama }}">{{ $item->nama }}</option>
                          @else
                            <option value="{{ $item->nama }}">{{ $item->nama }}</option>
                          @endif
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">No Urut</label>
                    <div class="col-sm-12 col-md-7">
                      <input type="text" class="form-control" name="no_urut" id="no_urut" placeholder="No Urut" value="{{ $data->no_urut }}" required>
                    </div>
                  </div>

                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Upload Image</label>
                    <div class="col-sm-12 col-md-7">
                      <input type="file" name="image" id="image-source" onchange="previewImage();"/>
                      <img id="image-preview" alt="imag e preview"/>
                    </div>
                  </div>


                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Visi</label>
                    <div class="col-sm-12 col-md-7">
                      <textarea class="summernote-simple" name="visi" style="width: 100%;height: 100px" placeholder="Visi">{{ $data->visi }}</textarea>
                    </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Misi</label>
                    <div class="col-sm-12 col-md-7">
                      <textarea class="summernote-simple" name="misi" style="width: 100%;height: 100px" placeholder="Misi">{{ $data->misi }}</textarea>
                    </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                    <div class="col-sm-12 col-md-7">
                      <a href="{{ url('/backend/candidates') }}" class="btn btn-danger">Cancel</a>
                      <button type="submit" name="submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <script type="text/javascript">

    function previewImage() {
      document.getElementById("image-preview").style.display = "block";
      var oFReader = new FileReader();
      oFReader.readAsDataURL(document.getElementById("image-source").files[0]);

      oFReader.onload = function(oFREvent) {
        document.getElementById("image-preview").src = oFREvent.target.result;
      };
    };
    $('#ketua').on('change', function(){
      var ketua = $(this).val();
      $.ajax({
        type : 'get',
        url : "{{url('/backend/candidates/show')}}",
        data : {'nama' : ketua},
        success : function(data){
          $('#wakil').html(data);
        }
      });
    });
  </script>
@stop
