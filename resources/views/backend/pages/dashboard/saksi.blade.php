<div class="table-responsive">
    <table class="table table-bordered table-md">
        <tr>
            <th>Jabatan</th>
            <th>Nama</th>
        </tr>
        <tr>
            <td>Ketua Pelaksana PILKAHIMAPSI</td>
            <td><span id="ketua_pelaksana">@if(isset($ketua_pelaksana))<b>({{$ketua_pelaksana->users->name}})</b>@endif</span></td>
        </tr>
        <tr>
            <td>Panwas 1</td>
            <td><span id="panwas1">@if(isset($panwas1))<b>({{$panwas1->users->name}})</b>@endif</span></td>
        </tr>
        <tr>
            <td>Panwas 2</td>
            <td><span id="panwas2">@if(isset($panwas2))<b>({{$panwas2->users->name}})</b>@endif</span></td>
        </tr>
        <tr>
            <td>Saksi Paslon 1</td>
            <td><span id="saksi_paslon1">@if(isset($saksi_paslon))<b>({{$saksi_paslon->users->name}})</b>@endif</span></td>
        </tr>
        <tr>
            <td>Saksi Paslon 2</td>
            <td><span id="saksi_paslon2">@if(isset($saksi_paslon))<b>({{$saksi_paslon->users->name}})</b>@endif</span></td>
        </tr>
    </table>
</div>
<div class="text-center">
    <button type="button" id="pengumuman" class="btn btn-success" name="button" style="display:none">Lihat Hasil Pengumuman</button>
</div>

<script type="text/javascript">

    var ketua_pelaksana = $('#ketua_pelaksana').text();
    var panwas1 = $('#panwas1').text();
    var panwas2 = $('#panwas2').text();
    var saksi_paslon = $('#saksi_paslon').text();

    var waktu = new Date();
    var batasWaktu = new Date(2020, 11, 6, 18, 0);
    // if(waktu > batasWaktu && ketua_pelaksana!='' && panwas1!='' && panwas2 != '' && saksi_paslon != '') {
        clearInterval(cek_saksi);
        $('#pengumuman').css("display", "");
        $('#pengumuman').click(function(){
            $.ajax(
                {
                    url : "{{url('/backend/dashboard/show')}}",
                    type : 'get',
                    data : {},
                    success : function(data){
                        $('#content').html(data);
                    }
                }
            );
        });
    // }


</script>
