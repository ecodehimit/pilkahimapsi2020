@extends('backend.master')
@section('title')
  Token
@endsection
@section('content')
  <div class="main-content">
    @if (\Session::has('success'))

      <div class="alert alert-success" role="alert">
        <a href="#" class="alert-link">{!! \Session::get('success') !!}</a>
      </div>
    @endif
      @if (\Session::has('danger'))

        <div class="alert alert-danger" role="alert">
          <a href="#" class="alert-link">{!! \Session::get('danger') !!}</a>
        </div>
      @endif
      @if (\Session::has('warning'))

        <div class="alert alert-warning" role="alert">
          <a href="#" class="alert-link">{!! \Session::get('warning') !!}</a>
        </div>
      @endif
    <section class="section">
      <div class="section-header">
        <h1>Token</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active">Master</div>
          <div class="breadcrumb-item"><a href="{{ url('/backend/token') }}">Token</a></div>
        </div>
      </div>


      <div class="section-body">
        <h2 class="section-title">Generate</h2>

        <div class="row">
          <div class="col-12">
            <div class="card">
              <form action="{{ url('/backend/token') }}" method="post">
                {{ csrf_field() }}
                  <div class="col-md-8">
                    <div class="form-group">
                      <label></label>
                      <select name="nrp" class="form-control select2" id="nrp">
                        @foreach($mahasiswa as $item)
                          <option value="{{ $item->nrp }}">{{ $item->nrp }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                    <div class="box-footer text-center">
                      <button type="submit" name="submit" class="btn btn-success">Generate</button>
                    </div>
              </form>



              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-striped" id="table">
                    <thead>
                    <tr>
                      <th>NIM</th>
                      <th>Token</th>
                      <th>Status Token</th>
                      <th>Durasi Token</th>
                      <th>Waktu Token</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
{{--                    @foreach($data as $item)--}}

{{--                      <tr>--}}
{{--                        <td>{{ $item->nrp }}</td>--}}
{{--                        <td>{{ $item->token }}</td>--}}
{{--                        <td>{{ $item->status_token }}</td>--}}
{{--                        <td class="timer" data-create="{{$item->updated_at}}" data-id="{{$item->id}}"></td>--}}
{{--                        <td>{{ $item->updated_at}}</td>--}}
{{--                        <td>--}}
{{--                          <form action="{{ url('backend/token/'.$item->id) }}" method="post">--}}
{{--                            {{ csrf_field() }}--}}
{{--                            <a href="{{url('backend/token/'.$item->id)}}" class="btn btn-primary">--}}
{{--                              <i class="fa fa-eye"></i>--}}
{{--                            </a>--}}
{{--                            {{ csrf_field() }}--}}
{{--                            <input type="hidden" name="_method" value="DELETE">--}}
{{--                            <input type="hidden" name="nrp" id="nrp" value="{{$item->nrp}}">--}}
{{--                              <a href="{{ url('backend/regenerate/token/'.$item->id) }}" onclick="return confirm('Are you sure?')" class="btn btn-info"><i class="fa fa-pencil"></i> Regenerate</a>--}}
{{--                            <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>--}}
{{--                          </form>--}}
{{--                        </td>--}}
{{--                      </tr>--}}
{{--                    @endforeach--}}

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

@endsection
@section('js')
  @parent()
  <script type="text/javascript">
    $(document).ready(function(){
      $('#table').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        processing: true,
        serverSide: true,
        ajax: {
          url: "{{ route('token') }}",
        },
        columns: [
          {
            data: 'nrp',
            name: 'nrp'
          },
          {
            data: 'token',
            name: 'token'
          },
          {
            data: 'status_token',
            name: 'status_token',
          },
          {
            data: 'durasi',
            name: 'durasi'
          },
          {
            data: 'updated_at',
            name: 'updated_at'
          },
          {
            data: 'action',
            name: 'action',
            orderable: false
          },
        ],
        "drawCallback": function( settings, json ) {
          // alert( 'DataTables has finished its initialisation.' );
          $('.timer').each(function(i){
            var timer = $(this);
            var id = $(this).attr('data-id');
            var time_in_minutes = 5;
            var current_time = Date.parse($(this).attr('data-create').replace(' ', 'T'));
            var deadline = new Date(current_time + time_in_minutes*60*1000);
            var status = $(this).attr('data-status');

            function time_remaining(endtime){
              var t = Date.parse(endtime) - Date.parse(new Date());
              var seconds = Math.floor( (t/1000) % 60 );
              var minutes = Math.floor( (t/1000/60) % 60 );
              var hours = Math.floor( (t/(1000*60*60)) % 24 );
              var days = Math.floor( t/(1000*60*60*24) );
              return {'total':t, 'days':days, 'hours':hours, 'minutes':minutes, 'seconds':seconds};
            }
            function run_clock(endtime){
              function update_clock(){
                var t = time_remaining(endtime);
                timer.text(t.minutes+'m'+t.seconds+'s');
                if(t.total<=0){
                  clearInterval(timeinterval);
                  timer.text("expired");
                  $.ajax({
                    type : 'put',
                    data : {id : id, _token : '{{csrf_token()}}'},
                    url : "{{url('/backend/token/update')}}",
                    success : function(data){
                      timer.prev().text('1');
                    }
                  });
                }

                if(status == '1'){
                  timer.text("expired");
                }

              }
              update_clock();
              var timeinterval = setInterval(update_clock,1000);
            }
            run_clock(deadline);
          });
        }
      });




    });

  </script>
@endsection
