@extends('backend.master')
@section('title')
  Administrator
@endsection
@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Master</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active">Master</div>
          <div class="breadcrumb-item"><a href="{{ url('/backend/users') }}">User</a></div>
        </div>
      </div>


      <div class="section-body">
        <h2 class="section-title">Users</h2>

        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4><a href="{{ url('/backend/users/create') }}" class="btn btn-primary">TAMBAH DATA</a> </h4>
              </div>

              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-striped" id="table-1">
                    <thead>
                    <tr>
                      <th>Nama</th>
                      <th>NIM</th>
                      <th>E-mail</th>
                      <th>Role</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $item)

                      <tr>
                      <td>{{ $item->nrp }}</td>
                      <td>
                        {{ $item->name }}
                      </td>
                      <td>
                        {{ $item->email }}
                      </td>
                      <td>
                        @if($item->role == 0)
                          <div class="badge badge-success">Admin</div>
                        @elseif($item->role == 1)
                          <div class="badge badge-warning">Panitia</div>
                        @elseif($item->role == 3)
                          <div class="badge badge-primary">Generator Token</div>
                        @endif

                      </td>
                      <td>
                        <form action="{{ url('backend/users/'.$item->id) }}" method="post">
                          {{ csrf_field() }}
                          <input type="hidden" name="_method" value="DELETE">
                          <a href="{{ url('backend/users/'.$item->id.'/edit') }}" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>
                          <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>
                        </form>
                      </td>
                    </tr>
                    @endforeach

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection


