@extends('backend.master')
@section('title')
  Tambah Admin
@endsection
@section('content')
  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Master</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active">Master</div>
          <div class="breadcrumb-item"><a href="{{ url('/backend/users') }}">Users</a></div>
        </div>
      </div>

      <div class="section-body">
        <h2 class="section-title">Users</h2>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4>Edit User</h4>
              </div>
              <form action="{{ url('/backend/users/'.$data->id) }}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT">
                <div class="card-body">
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">NIM</label>
                    <div class="col-sm-12 col-md-7">
                      <select class="form-control selectric" id="nrp" name="nrp">
                        @foreach($mahasiswa as $item)
                          @if($data->nrp == $item->nrp)
                            <option value="{{ $item->nrp }}" selected>{{ $item->nrp }}</option>
                          @else
                            <option value="{{ $item->nrp }}">{{ $item->nrp }}</option>
                          @endif
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
                    <div class="col-sm-12 col-md-7">
                      <input type="text" name="name" id="name" class="form-control" value="{{$data->name}}" readonly>
                    </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">E-mail</label>
                    <div class="col-sm-12 col-md-7">
                      <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="{{ $data->email }}" required>
                    </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Password</label>
                    <div class="col-sm-12 col-md-7">
                      <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                    </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Role</label>
                    <div class="col-sm-12 col-md-7">
                      <select name="role" class="form-control selectric">
                          @if($data->role == '3')
                            <option value="3" selected>Generate Token</option>
                          @else
                            <option value="3">Generate Token</option>
                          @endif
                      </select>
                    </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                    <div class="col-sm-12 col-md-7">
                      <a href="{{ url('/backend/users') }}" class="btn btn-danger">Cancel</a>
                      <button type="submit" name="submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <script type="text/javascript">
    $(document).ready(function(){
      $('#nrp').on('change', function(){
        var nrp = $(this).val();
        $.ajax({
          type : 'get',
          url : "{{ url('/backend/users/show') }}",
          data : {"nrp" : nrp},
          success : function(data){
            $('#name').val(data);
          }
        });
      });
    });
  </script>
@stop
