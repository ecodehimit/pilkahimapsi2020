@extends('backend.master')
@section('title')
  Manage Device
@endsection
@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Master</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active">Master</div>
          <div class="breadcrumb-item"><a href="{{ url('/backend/devices') }}">Devices</a></div>
        </div>
      </div>


      <div class="section-body">
        <h2 class="section-title">Device</h2>

        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4>List Device</h4>
              </div>

              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-striped" id="table-1">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>IP Address</th>
                      <th>Device Name</th>
                      <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $item)

                      <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->ip_address }}</td>
                        <td>{{ $item->device_name }}</td>
                        <td>
                          @if($item->active_client == 'Aktif')
                            <div class="badge badge-success">admin</div>
                          @else
                            <span class="btn btn-danger">{{ $item->active_client }}</span>

                          @endif

                        </td>
                        <td>{{ $item->time }}</td>

                      </tr>
                    @endforeach

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection

@section('js')
  @parent
  <script type="text/javascript">
    $(document).ready(function(){
      $('#table').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false
      });
    });

    setInterval(function () {
      $.ajax({
        url : "{{url('/backend/devices/update')}}",
        type : 'GET',
        data : {},
        success : function(data){
          $('#data-table').html(data);
        }
      });
    }, 5000);

  </script>

@endsection
