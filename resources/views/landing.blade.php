<!DOCTYPE html>
<html lang="en">
<head>
    {{--    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>--}}
    <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{asset('img/evote_logo.png')}}">
    <title>E-Vote PILKAHIMAPSI UB 2021</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- template-->
    <link rel="stylesheet" href="{{asset('css/')}}">
    <link rel="stylesheet" href="{{asset('css/semantic.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/icon.min.css')}}">
    <script src="{{asset('js/semantic.js')}}"></script>


    <!-- Template CSS -->   <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link rel="stylesheet" href="{{url('css/material-kit.css')}}">

    <style>
        img{
            height: 75px;
            width: 75px;
            margin: 13px 0 10px 30px
        }
        .card{
            background-color: #6E0081;
            color: white;
            height: 142px;
            border: 5px solid #F5B910 ;
            padding: 0;
            margin: 30px;
        }
        .card-number{
            font-weight: bold;
            font-size: 26px;
            margin-bottom: 0px;
        }
        .card-content{
            font-weight: 400;
            font-size: 12px;
            margin-top:10px
        }
    </style>
</head>

{{--<body style="background-image: url('{{asset('img/bg-web.png')}}');">--}}

<body style="background-color:#6E0081;">
<div style="background-color:#323232;margin: 20px;height: 100vh">

    <center>
        <div class="header-landing">
            <div class="logo-landing">
                <img src=img/logo_ub.png>
                <img src=img/logo_himapsi.png>
            </div>
        </div>
    </center>


    <div class="content-landing align-self-center text-center" style="margin-top: 100px">
        <h1 style="color: white; font-weight: bold; margin: 0">PILKAHIMAPSI 2022</h1>
        <h4 style="color: white; font-weight: bold; margin: 0">Pemilihan Ketua dan Wakil Ketua Himpunan Psikologi</h4>

        <div class="col align-self-center text-center" style="margin: 80px 0 140px 0;">
            {{--            <h2 style="color: #f54610; font-weight: bold;">WAKTU VOTING SUDAH BERAKHIR</h2>--}}
            <a href="{{url('/vote')}}" class="btn btn-primary btn-lg .btn-vote" style="background-color: #F5B910; font-weight: bold; color: #6E0081">
                Let's vote
            </a>
        </div>

        <br>
            <div class="footer-landing align-self-center text-center">
{{--                <p class="card-content" style="color: white; letter-spacing: 2px; font-weight: bold">--}}
{{--                    Narahubung RSVP: Meila (0823-3838-2734/meyla.12), Athaya (0858-4827-7628/athayaaf), <br>--}}
{{--                    Dyah (0813-3624-3340/keynn1), Zahra (0858-4112-0523/zaracvwfmccann), Naura(0819-9898-1788/raranabilaa)--}}
{{--                </p>--}}
                <p class="card-content" style="color: white; letter-spacing: 2px; font-weight: bold;font-size: 30px">
                    HIMAPSI UB
                </p>
            </div>
    </div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
