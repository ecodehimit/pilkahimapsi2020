<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSaksiTableSeeder extends Seeder
{
    public function run()
    {
        $data = array(
            array(
                'nrp' => '205120301111005',
                'name' => 'Surya Wiralegawa',
                'email' => 'saksi@evotelab.com',
                'password' => Hash::make('w12es'),
                'role' => '2',
                'role_web' => 'saksi',
                'created_at' => now(),
                'updated_at' => now(),
            ),
            array(
                'nrp' => '185120300111020',
                'name' => 'Putri Salma Salsabilah',
                'email' => 'saksi@evotelab.com',
                'password' => Hash::make('1gqf3'),
                'role' => '2',
                'role_web' => 'saksi',
                'created_at' => now(),
                'updated_at' => now(),
            ),
            array(
                'nrp' => '205120301111005',
                'name' => 'Surya Wiralega',
                'email' => 'saksi@evotelab.com',
                'password' => Hash::make('adyq1'),
                'role' => '2',
                'role_web' => 'saksi',
                'created_at' => now(),
                'updated_at' => now(),
            ),
            array(
                'nrp' => '215120300111016',
                'name' => 'Guruh Prasetyo Bagaskoro',
                'email' => 'saksi@evotelab.com',
                'password' => Hash::make('wqwe3'),
                'role' => '2',
                'role_web' => 'saksi',
                'created_at' => now(),
                'updated_at' => now(),
            ),
            array(
                'nrp' => '195120301111049',
                'name' => 'Safira Asrininingwuri',
                'email' => 'saksi@evotelab.com',
                'password' => Hash::make('xsv13'),
                'role' => '2',
                'role_web' => 'saksi',
                'created_at' => now(),
                'updated_at' => now(),
            ),
            array(
                'nrp' => '205120307111053',
                'name' => 'Alexander Ivan Gunawan',
                'email' => 'saksi@evotelab.com',
                'password' => Hash::make('cbda1'),
                'role' => '2',
                'role_web' => 'saksi',
                'created_at' => now(),
                'updated_at' => now(),
            ),
        );
        User::insert($data);
    }
}
