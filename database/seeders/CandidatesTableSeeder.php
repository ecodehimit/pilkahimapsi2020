<?php

namespace Database\Seeders;

use App\Models\Candidates;
use Illuminate\Database\Seeder;

class CandidatesTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'ketua' => 'calon ketua1',
                'wakil' => 'calon wakil ketua1',
                'no_urut' => '1',
                'photo' => 'aaaaaaaaaaaaaaaa',
                'visi' => '-',
                'misi' => '--',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'ketua' => 'calon ketua2',
                'wakil' => 'calon wakil ketua2',
                'no_urut' => '2',
                'photo' => '-',
                'visi' => '--',
                'misi' => '--',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'ketua' => 'Tidak Sah',
                'wakil' => '-',
                'no_urut' => '-1',
                'photo' => '-',
                'visi' => '-',
                'misi' => '-',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ];

        Candidates::insert($data);
    }
}
