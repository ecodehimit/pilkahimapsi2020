<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $data = array(
//            array(
//                'nrp' => '2110181015',
//                'name' => 'Rochimatus Sa\'diyah',
//                'email' => 'rochimatus@evotelab.com',
//                'password' => Hash::make('iimmm'),
//                'role' => '0',
//                'role_web' => 'admin',
//                'created_at' => now(),
//                'updated_at' => now(),
//            ),
            array(
                'nrp' => '2103191001',
                'name' => 'Rizky Putra Ramadan',
                'email' => 'putra@evotelab.com',
                'password' => Hash::make('rizky'),
                'role' => '0',
                'role_web' => 'admin',
                'created_at' => now(),
                'updated_at' => now(),
            ),
//            array(
//                'nrp' => '195120301111051',
//                'name' => 'Nova Herliana Sinaga',
//                'email' => 'pilkahimapsi@evotelab.com',
//                'password' => Hash::make('pilkahimapsi21'),
//                'role' => '0',
//                'role_web' => 'admin',
//                'created_at' => now(),
//                'updated_at' => now(),
//            ),
//            array(
//                'nrp' => '205120307111036',
//                'name' => 'Wildan Akhsanul Iman',
//                'email' => 'panitia@evotelab.com',
//                'password' => Hash::make('ssaw1'),
//                'role' => '3',
//                'role_web' => 'gen-token',
//                'created_at' => now(),
//                'updated_at' => now(),
//            ),
//            array(
//                'nrp' => '22222',
//                'name' => 'ini panitia gen',
//                'email' => 'panitia@evotelab.com',
//                'password' => Hash::make('axv234'),
//                'role' => '3',
//                'role_web' => 'gen-token',
//                'created_at' => now(),
//                'updated_at' => now(),
//            ),
//            array(
//                'nrp' => '11111',
//                'name' => 'ini panitia gen',
//                'email' => 'panitia@evotelab.com',
//                'password' => Hash::make('iad28'),
//                'role' => '3',
//                'role_web' => 'gen-token',
//                'created_at' => now(),
//                'updated_at' => now(),
//            ),
        );
        User::insert($data);
    }
}
