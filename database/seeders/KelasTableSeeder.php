<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KelasTableSeeder extends Seeder
{
    public function run()
    {
        $result = DB::table('kelas')->insert([
            'tahun' => '2019',
            'tingkat' => '1',
            // 'jenjang' => 'D3',
            // 'kelas' => 'A',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        $result = DB::table('kelas')->insert([
            'tahun' => '2019',
            'tingkat' => '1',
            // 'jenjang' => 'D3',
            // 'kelas' => 'B',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $result = DB::table('kelas')->insert([
            'tahun' => '2019',
            'tingkat' => '1',
            // 'jenjang' => 'D4',
            // 'kelas' => 'A',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        $result = DB::table('kelas')->insert([
            'tahun' => '2019',
            'tingkat' => '1',
            // 'jenjang' => 'D4',
            // 'kelas' => 'B',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        $result = DB::table('kelas')->insert([
            'tahun' => '2018',
            'tingkat' => '2',
            // 'jenjang' => 'D3',
            // 'kelas' => 'A',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        $result = DB::table('kelas')->insert([
            'tahun' => '2018',
            'tingkat' => '2',
            // 'jenjang' => 'D3',
            // 'kelas' => 'B',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $result = DB::table('kelas')->insert([
            'tahun' => '2017',
            'tingkat' => '2',
            // 'jenjang' => 'D4',
            // 'kelas' => 'A',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        $result = DB::table('kelas')->insert([
            'tahun' => '2017',
            'tingkat' => '2',
            // 'jenjang' => 'D4',
            // 'kelas' => 'B',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        $result = DB::table('kelas')->insert([
            'tahun' => '2017',
            'tingkat' => '2',
            // 'jenjang' => 'D3',
            // 'kelas' => 'A',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        $result = DB::table('kelas')->insert([
            'tahun' => '2017',
            'tingkat' => '2',
            // 'jenjang' => 'D3',
            // 'kelas' => 'B',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $result = DB::table('kelas')->insert([
            'tahun' => '2018',
            'tingkat' => '2',
            // 'jenjang' => 'D4',
            // 'kelas' => 'A',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        $result = DB::table('kelas')->insert([
            'tahun' => '2018',
            'tingkat' => '2',
            // 'jenjang' => 'D4',
            // 'kelas' => 'B',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        $result = DB::table('kelas')->insert([
            'tahun' => '2017',
            'tingkat' => '3',
            // 'jenjang' => 'D3',
            // 'kelas' => 'A',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        $result = DB::table('kelas')->insert([
            'tahun' => '2017',
            'tingkat' => '3',
            // 'jenjang' => 'D3',
            // 'kelas' => 'B',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        $result = DB::table('kelas')->insert([
            'tahun' => '2017',
            'tingkat' => '3',
            // 'jenjang' => 'D4',
            // 'kelas' => 'A',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        $result = DB::table('kelas')->insert([
            'tahun' => '2017',
            'tingkat' => '3',
            // 'jenjang' => 'D4',
            // 'kelas' => 'B',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        $result = DB::table('kelas')->insert([
            'tahun' => '2016',
            'tingkat' => '4',
            // 'jenjang' => 'D4',
            // 'kelas' => 'A',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        $result = DB::table('kelas')->insert([
            'tahun' => '2016',
            'tingkat' => '4',
            // 'jenjang' => 'D4',
            // 'kelas' => 'B',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
