<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateQToken extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE VIEW q_token AS SELECT token.*, mahasiswa.nama FROM token INNER JOIN mahasiswa ON token.nrp = mahasiswa.nrp');
        DB::statement('CREATE VIEW q_devices AS SELECT a.*, m.nama FROM devices as a INNER JOIN mahasiswa as m ON a.nrp = m.nrp');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW q_token');
        DB::statement('DROP VIEW q_devices');
    }
}
