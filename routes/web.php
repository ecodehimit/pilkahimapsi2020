<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/server', function(){
    return view('frontend.button');
});

Route::get('admin', 'Auth\LoginController@loginAdmin')->middleware('role_web')->name('login.admin');
Route::get('/vote', function () {
    return view('voting.login');
});

Route::get('/', function () {
    return view('landing');
});

 Route::post('/vote', 'LoginsController@login');

//Route::post('/voting', 'LoginController@login');

Route::post('/pilih', 'LoginsController@pilih');
Route::get('/berhasil', 'LoginsController@berhasil');

Route::post('/auth/login', ['as' => 'login', 'uses' => 'Auth\LoginController@login']);
Route::get('/auth/logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

Route::group(['prefix' => 'backend', 'namespace' => 'Backend', 'middleware' => ['auth', 'role_web:admin' ]], function() {
    Route::get('/mahasiswas/import', 'MahasiswaController@toImport')->name('backend.mahasiswa.toImport');
    Route::get('/mahasiswas/export', 'MahasiswaController@export')->name('backend.mahasiswa.export');
    Route::post('mahasiswas/import', 'MahasiswaController@import')->name('backend.mahasiswa.import');
    Route::get('/', array('as' => 'backend.dashboard.index', 'uses' => 'DashboardController@index'));
    Route::resource('users', 'UsersController');
    Route::resource('kelas', 'KelasController');
    Route::resource('mahasiswa', 'MahasiswaController');
    Route::resource('candidates', 'CandidatesController');
    Route::resource('token', 'TokenController');
    Route::resource('logs_web', 'LogsWebController');
    Route::resource('devices', 'DevicesController');
    Route::resource('suara', 'SuaraController');
    Route::resource('dashboard', 'DashboardController');
    Route::get('regenerate/token/{id}', 'TokenController@regenerate');

});
Route::get('/backend/token', 'Backend\TokenController@index')->name('token');
Route::get('/backend/mahasiswas', 'Backend\MahasiswaController@index')->name('mahasiswa');
Route::get('/backend/mahasiswa/des/{id}', 'Backend\MahasiswaController@destroy')->name('mahasiswadestroy');
Route::get('/backend/delete-token/des/{id}', 'Backend\TokenController@destroy')->name('tokendestroy');

Route::group(['prefix' => 'saksi', 'namespace' => 'Saksi', 'middleware' => ['auth', 'role_web:saksi' ]], function() {
    Route::get('/', 'DashboardController@index')->name('saksi.dashboard.index');
    Route::resource('suara', 'SuaraController');
    Route::resource('dashboard', 'DashboardController');
    Route::resource('mahasiswa', 'MahasiswaController');
    Route::resource('dashboard', 'DashboardController');
});

Route::group(['prefix' => 'gen-token', 'namespace' => 'GenToken', 'middleware' => ['auth', 'role_web:gen-token' ]], function() {
    Route::get('/', 'TokenController@index')->name('gen-token.dashboard.index');
    Route::resource('token', 'TokenController', ['as' => 'token']);
    Route::get('regenerate/token/{id}', 'TokenController@regenerate');
    Route::resource('dashboard', 'DashboardController');
    Route::get('regenerate/token/{id}', 'TokenController@regenerate');
});
Route::get('/bem/token', 'GenToken\TokenController@index')->name('tokenbem');

Route::get('validasi/{nrp}', 'GetmahasiswaController@validasi');

Route::group(['prefix' => 'backend', 'namespace' => 'Backend'], function() {
    Route::get('/token/show/{id}', ['as' => 'backend.token.show', 'uses' => 'TokenController@show']);
});

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});
