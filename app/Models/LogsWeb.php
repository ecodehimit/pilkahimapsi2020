<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogsWeb extends Model
{
    use SoftDeletes;

    protected $table = 'logs_web';

    protected $fillable = [
        'users_id',
        'nrp',
        'time',
    ];

    public function users()
    {
        return $this->belongsTo('App\Models\User', 'users_id')->withTrashed();
    }
}
