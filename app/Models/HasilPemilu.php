<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HasilPemilu extends Model
{
    protected $table = 'hasil_pemilu';

    protected $fillable = [
        'nrp',
        'candidates_id',
    ];

    public function candidates()
    {
        return $this->belongsTo('App\Models\Candidates', 'candidates_id')->withTrashed();
    }
}
