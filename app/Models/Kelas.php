<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kelas extends Model
{
//    use SoftDeletes;

    protected $table = 'kelas';

    protected $fillable = [
        'tahun',
    ];

//    protected $guarded= [];
    public function mahasiswa_informatika()
    {
        return $this->hasMany('App\Models\MahasiswaInformatika', 'kelas_id');
    }
}
