<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mahasiswa extends Model
{
    use SoftDeletes;

    protected $table = 'mahasiswa';

    protected $fillable = [
        'kelas_id',
        'nrp',
        'nama',
        'token',
        'status_token',
        'status_pilih',
        'voted_at',
    ];

//    protected $with = ['kelas'];

    public function kelas()
    {
        return $this->belongsTo('App\Models\Kelas', 'kelas_id');
    }

    public function token()
    {
        return $this->hasOne('App\Models\Token', 'nrp');
    }
}
