<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Candidates extends Model
{
    use SoftDeletes;

    protected $table = 'candidates';

    protected $fillable = [
        'ketua',
        'wakil',
        'no_urut',
        'photo',
        'visi',
        'misi',
    ];
}
