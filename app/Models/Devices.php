<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Devices extends Model
{
    protected $table = 'devices';

    protected $fillable = [
        'nrp',
        'ip_address',
        'device_name',
        'active_client',
    ];
}
