<?php

namespace App\Imports;

use App\Models\Kelas;
use App\Models\Mahasiswa;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class MahasiswaImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
//        dd($rows);
        foreach ($rows as $row) {
//            Kelas::firstOrCreate(
//                ['tahun' => $row['tahun']]
//            );
//
//            $kelas = Kelas::firstOrCreate(
//                ['tahun' => $row['tahun']]
//            );

            $kelas = Kelas::where('tahun',$row['tahun'])->first();


            Mahasiswa::firstOrCreate(
                [
                    'nrp' => $row['nim']
                ], [
                    'nama' => $row['nama'],
                    'kelas_id' => $kelas->id,
                ]
            );
        }
    }
}
