<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // Force generating https in url helper
        if (config('app.env') == 'production') {
            URL::forceScheme('https');
        }

        // Override server timezome to match local time (Indonesia)
        setlocale(LC_TIME, 'id_ID.utf8');
        Carbon::setLocale(config('app.locale'));

        // Force using bootstrap for pagination
        Paginator::useBootstrap();
    }
}
