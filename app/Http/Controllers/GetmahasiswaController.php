<?php

namespace App\Http\Controllers;

use App\Models\Mahasiswa;
use Illuminate\Http\Request;

class GetmahasiswaController extends Controller
{
    public function validasi(Request $request, $nrp)
    {
        $mahaiswa = Mahasiswa::where('nrp', $nrp)->first();
        if ('Sudah' == $mahaiswa->status_pilih) {
            return response()->json(true);
        }

        return response()->json(false);
    }
}
