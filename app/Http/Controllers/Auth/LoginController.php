<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\LogsWeb;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function username()
    {
        return 'nrp';
    }

    public function loginAdmin()
    {
        return view('login.landing');
    }

    public function redirectTo()
    {
        $role = auth()->user()->role_web;

        switch ($role) {
            case 'admin':
                return route('backend.dashboard.index');

                break;
            case 'panitia':
                return route('panitia.dashboard.index');

                break;
            case 'saksi':
                return route('saksi.dashboard.index');

                break;
            case 'gen-token':
                return route('gen-token.dashboard.index');

                break;
            default:
                return '/admin';
                break;
        }
    }

    public function messages()
    {
        return [
            'nrp.required' => 'NRP is required',
            'password.required' => 'Password is required',
        ];
    }

    protected function authenticated(Request $request, $user)
    {
        $user = User::where('nrp', $request->nrp)->first();
        LogsWeb::firstOrCreate([
            'users_id' => $user->id,
            'nrp' => $request->nrp,
            'time' => Carbon::now(),
        ]);
    }
}
