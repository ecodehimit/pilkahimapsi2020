<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Candidates;
use App\Models\HasilPemilu;
use App\Models\LogsWeb;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('backend.pages.dashboard.index');
    }

    public function update()
    {
        $data = [
            'ketua_pelaksana' => LogsWeb::with('users')->where('users_id', '25')->first(),
            'panwas1' => LogsWeb::with('users')->where('users_id', '26')->first(),
            'panwas2' => LogsWeb::with('users')->where('users_id', '27')->first(),
            'saksi_paslon1' => LogsWeb::with('users')->where('users_id', '28')->first(),
            'saksi_paslon2' => LogsWeb::with('users')->where('users_id', '29')->first(),

        ];

        return view('backend.pages.dashboard.saksi', $data);
    }

    public function show()
    {
        $calon1 = Candidates::where('no_urut', 1)->first();
        $calon2 = Candidates::where('no_urut', 2)->first();
        $totalcalon1 = HasilPemilu::where('candidates_id', $calon1->id)->get();
        $totalcalon2 = HasilPemilu::where('candidates_id', $calon2->id)->get();
        $data = [
            'total1' => count($totalcalon1),
            'total2' => count($totalcalon2),
            'calon1' => $calon1,
            'calon2' => $calon2,
        ];

        return view('backend.pages.dashboard.pengumuman', $data);
    }
}
