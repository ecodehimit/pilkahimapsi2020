<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Mahasiswa;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function index()
    {
        $data['data'] = User::orderBy('id', 'DESC')->get();

        return view('backend.pages.users.index', $data);
    }

    public function create()
    {
        $data['mahasiswa'] = Mahasiswa::get();

        return view('backend.pages.users.create', $data);
    }

    public function store(Request $request)
    {
        $req = $request->all();

        if ('0' == $req['role']) {
            $req['role_web'] = 'admin';
        } elseif ('1' == $req['role']) {
            $req['role_web'] = 'panitia';
        } elseif('3' == $req['role']) {
            $req['role_web'] = 'gen-token';
        }

        $req['password'] = Hash::make($req['password']);

        $nrp = $req['nrp'];
        $count = User::where('nrp', $nrp)->count();

        if ($count > 0) {
            return redirect()->back()->withInput()->with('message', [
                'title' => 'Oops!',
                'type' => 'danger',
                'msg' => 'NRP has been registered',
            ]);
        }
        $result = User::create($req);

        return redirect('backend/users')->withInput()->with('message', [
            'title' => 'Yay!',
            'type' => 'success',
            'msg' => 'Saved Success.',
        ]);
    }

    public function edit($id)
    {
        $data['data'] = User::find($id);
        $data['mahasiswa'] = Mahasiswa::get();

        return view('backend.pages.users.edit', $data);
    }

    public function update($id, Request $request)
    {
        $req = $request->except('_method', '_token', 'submit', 'licenses_id');

        if ('0' == $req['role']) {
            $req['role_web'] = 'admin';
        } else {
            $req['role_web'] = 'panitia';
        }

        $req['password'] = Hash::make($req['password']);

        $result = User::where('id', $id)->update($req);

        return redirect('backend/users')->withInput()->with('message', [
            'title' => 'Yay!',
            'type' => 'success',
            'msg' => 'Saved Success.',
        ]);
    }

    public function destroy($id)
    {
        $result = User::find($id);
        $result->Delete();

        return redirect('backend/users')->withInput()->with('message', [
            'title' => 'Yay!',
            'type' => 'success',
            'msg' => 'Deleted data.',
        ]);
    }

    public function show(Request $req)
    {
        $nrp = $req['nrp'];
        $result = Mahasiswa::where('nrp', $nrp)->first();
        echo $result->nama;
    }
}
