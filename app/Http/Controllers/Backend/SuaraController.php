<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\HasilPemilu;
use App\Models\Mahasiswa;

class SuaraController extends Controller
{
    public function index()
    {
        //        $total = MahasiswaInformatika::count();
        //        $tokenGenerated = Token::query()->count();
        //        $suaraMasuk = MahasiswaInformatika::where('status_pilih', '!=', 'Belum')->count();
        //        $suaraBelum = $total - $suaraMasuk;
        //        $masuk = ($suaraMasuk/$total) * 100;
        //        $hasToken = ($tokenGenerated/$total) * 100;
        //        $belum = (($total - $suaraMasuk) / $total ) *100;
        $total = Mahasiswa::count();
        $tokenGenerated = HasilPemilu::query()->count();

        if (0 == $tokenGenerated) {
            $suaraBelum = $total - $tokenGenerated;
            $masuk = 0;
            $belum = 0;

            $data = [
                'masuk' => 0,
                'belum' => 0,
                'tokenGenerated' => $tokenGenerated,
                'tokenGeneratedBelum' => $total - $tokenGenerated,
                //            'has_token' => $hasToken
            ];
        } else {
            $suaraBelum = $total - $tokenGenerated;
            $masuk = ($tokenGenerated / $total) * 100;
            $belum = (($total - $tokenGenerated) / $total) * 100;

            $data = [
                'masuk' => $masuk,
                'belum' => $belum,
                'tokenGenerated' => $tokenGenerated,
                'tokenGeneratedBelum' => $total - $tokenGenerated,
                //            'has_token' => $hasToken
            ];
        }

        return view('backend.pages.suara_masuk.index', $data);
    }
}
