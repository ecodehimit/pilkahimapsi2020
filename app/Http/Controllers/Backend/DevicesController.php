<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Devices;

class DevicesController extends Controller
{
    public function index()
    {
        $data['data'] = Devices::orderBy('id', 'ASC')->get();

        return view('backend.pages.devices.index', $data);
    }

    public function show()
    {
        $data['data'] = Devices::orderBy('id', 'ASC')->get();

        return view('backend.pages.devices.list', $data);
    }
}
