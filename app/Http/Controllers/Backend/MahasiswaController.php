<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Mahasiswa;
use Yajra\DataTables\Datatables;
use App\Models\Kelas;
use App\Exports\MahasiswaExport;
use App\Imports\MahasiswaImport;
use Maatwebsite\Excel\Facades\Excel;

class MahasiswaController extends Controller
{
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $data = Mahasiswa::with('kelas')->orderBy('id', 'DESC');
            // $data = Mahasiswa::latest()->get();
            $datatables = DataTables::of($data)
                ->addColumn('kelas', function ($data) {
                    $p = $data->kelas->tahun; //.$data->kelas->jenjang.' '.$data->kelas->kelas;
//                    $p = 'sa'; //.$data->kelas->jenjang.' '.$data->kelas->kelas;
                    return $p;
                })
                ->addColumn('action', function ($data) {
                    $button = '<a href="mahasiswa/'.$data->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                    $button .= '<a href="'.route('mahasiswadestroy', $data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';

                    return $button;
                })
                ->rawColumns(['kelas', 'action'])
                ->make(true);
//            dd($datatables);
            return $datatables;
        }

        return view('backend.pages.mahasiswa.index');
    }

    public function create()
    {
        $data['kelas'] = Kelas::get();
        // dd($data);

        return view('backend.pages.mahasiswa.create', $data);
    }

    public function store(Request $request)
    {
        $req = $request->all();

        $result = Mahasiswa::create($req);

        return redirect('backend/mahasiswas')->withInput()->with('message', [
            'title' => 'Yay!',
            'type' => 'success',
            'msg' => 'Saved Success.',
        ]);
    }

    public function edit($id)
    {
        $data['kelas'] = Kelas::get();
        $data['data'] = Mahasiswa::find($id);

        return view('backend.pages.mahasiswa.edit', $data);
    }

    public function update($id, Request $request)
    {
        $req = $request->except('_method', '_token', 'submit');

        $result = Mahasiswa::where('id', $id)->update($req);

        return redirect('backend/mahasiswa')->withInput()->with('message', [
            'title' => 'Yay!',
            'type' => 'success',
            'msg' => 'Saved Success.',
        ]);
    }

    public function destroy($id)
    {
        $result = Mahasiswa::find($id);
        $result->delete();

        return redirect('backend/mahasiswas')->withInput()->with('message', [
            'title' => 'Yay!',
            'type' => 'success',
            'msg' => 'Deleted data.',
        ]);
    }

    public function toImport()
    {
        return view('backend.pages.mahasiswa.import');
    }

    public function import(Request $request)
    {
        $request->validate([
            'excel' => 'required|mimes:xlsx',
        ]);

//        dd($request->excel);

        Excel::import(new MahasiswaImport, $request->excel);
        return redirect('backend/mahasiswas')
            ->with('success', 'Data imported successfully.')
        ;
    }

    public function export()
    {
        return Excel::download(new MahasiswaExport, 'pilkahimapsi2020.xlsx');
    }
}
