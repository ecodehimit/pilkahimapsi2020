<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Mahasiswa;
use App\Models\Token;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Datatables;

class TokenController extends Controller
{
    public function index(Request $request)
    {
        $data['data'] = Token::orderBy('id', 'DESC')->get();
        $data['mahasiswa'] = Mahasiswa::where('status_pilih', '=', 'Belum')->get();
        if ($request->ajax()) {
            $token = Token::orderBy('id', 'DESC')->get();

            return DataTables::of($token)
                ->addColumn('durasi', function ($data) {
                    return '<p class="timer" data-create="'.$data->updated_at.'" data-id="'.$data->id.'" data-status="'.$data->status_token.'"></p>';
                })
                ->addColumn('action', function ($data) {
                    $button = '<a href="'.url('backend/regenerate/token/'.$data->id).'" onclick="return confirm('.'`Are you sure?`'.')" class="btn btn-info"><i class="fa fa-pencil"></i> Regenerate</a>';
                    //                    $button .= '<a href="' . route('mahasiswadestroy', $data->id) . '" class="btn btn-danger" onclick="return confirm('.'Are you sure?'.')"><i class="fa fa-trash"></i> Delete</a>';
                    $button .= '<a href="'.url('/backend/delete-token/des/'.$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';

                    return $button;
                })
                ->rawColumns(['durasi', 'action'])
                ->make(true)
            ;
        }

        return view('backend.pages.token.index', $data);
    }

    public function store(Request $request)
    {
        $req = $request->all();

        $random = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $req['token'] = substr(str_shuffle(str_repeat($random, 5)), 0, 5);

        $req['status_token'] = 0;

        $nrp = $req['nrp'];
        $count = Token::where('nrp', $nrp)->count();
        $cek_mahasiswa = Mahasiswa::where([['nrp', '=', $nrp], ['status_pilih', '=', 'Sudah']])->count();

        if ($count > 0 || $cek_mahasiswa > 0) {
            if ($cek_mahasiswa > 0) {
                return redirect()->back()->with('danger', 'Kamu sudah menggunakan hak pilihmu');
            }

            return redirect()->back()->with('warning', 'Kamu sudah punya token');
        }
        $result = Token::query()->create($req);

        return redirect('backend/token/show/'.$result->id)->with('success', 'Token berhasil dibuat');
    }

    public function show($id)
    {
        $data['data'] = DB::table('q_token')->where('id', $id)->first();

        return view('backend.pages.token.show', $data);
    }

    public function edit($id)
    {
        $data['data'] = Token::find($id);

        return view('backend.pages.token.edit', $data);
    }

    public function update($id, Request $request)
    {
        $id = $request['id'];
        Token::where('id', $id)->update(['status_token' => 1]);
    }

    public function destroy($id)
    {
        $result = Token::find($id);
        $result->delete();

        return redirect()->back()->with('danger', 'Deleted data');
    }

    public function regenerate(Request $request, $id)
    {
        $req = $request->all();

        $random = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $new_token = substr(str_shuffle(str_repeat($random, 5)), 0, 5);

        $status_token = 0;
        //
        //        $nrp = $req['nrp'];
        $count = Token::where('id', $id)->count();
        $token = Token::where('id', $id)->first();

        $mahasiswa = Mahasiswa::where('nrp', $token->nrp)->first();

        if ('Sudah' == $mahasiswa->status_pilih) {
            return redirect()->back()->with('danger', 'Kamu sudah menggunakan hak pilihmu');
        }
        if ($token->count < 2) {
            $update = Token::where('id', $id)->update(['count' => $token->count + 1, 'token' => $new_token, 'status_token' => $status_token]);

            return redirect()->back()->with('success', 'Berhasil melakukan regenerate token');
        }

        return redirect()->back()->with('danger', 'Kamu sudah 2 kali generate token');
    }
}
