<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\LogsWeb;

class LogsWebController extends Controller
{
    public function index()
    {
        $data['data'] = LogsWeb::with('users')->orderBy('id', 'DESC')->get();

        return view('backend.pages.logs_web.index', $data);
    }
}
