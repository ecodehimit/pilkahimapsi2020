<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Kelas;
use Illuminate\Http\Request;

class KelasController extends Controller
{
    public function index()
    {
        $data = [
            'data' => Kelas::orderBy('id', 'DESC')->get(),
        ];

        return view('backend.pages.kelas.index', $data);
    }

    public function create()
    {
        return view('backend.pages.kelas.create');
    }

    public function store(Request $request)
    {
        $req = $request->all();

        $result = Kelas::create($req);

        return redirect('backend/kelas')->withInput()->with('message', [
            'title' => 'Yay!',
            'type' => 'success',
            'msg' => 'Saved Success.',
        ]);
    }

    public function edit($id)
    {
        $data['data'] = Kelas::find($id);

        return view('backend.pages.kelas.edit', $data);
    }

    public function update($id, Request $request)
    {
        $req = $request->except('_method', '_token', 'submit');

        $result = Kelas::where('id', $id)->update($req);

        return redirect('backend/kelas')->withInput()->with('message', [
            'title' => 'Yay!',
            'type' => 'success',
            'msg' => 'Saved Success.',
        ]);
    }

    public function destroy($id)
    {
        $result = Kelas::find($id);
        $result->delete();

        return redirect('backend/kelas')->withInput()->with('message', [
            'title' => 'Yay!',
            'type' => 'success',
            'msg' => 'Deleted data.',
        ]);
    }
}
