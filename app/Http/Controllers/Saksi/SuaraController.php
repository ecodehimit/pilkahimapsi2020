<?php

namespace App\Http\Controllers\Saksi;

use App\Http\Controllers\Controller;
use App\Models\Mahasiswa;
use App\Models\Token;

class SuaraController extends Controller
{

    public function index()
    {
        $total = Mahasiswa::count();
        $tokenGenerated = Token::query()->count();
        $suaraBelum = $total - $tokenGenerated;
        $masuk = ($tokenGenerated / $total) * 100;
        $belum = (($total - $tokenGenerated) / $total) * 100;

        $data = [
            'masuk' => $masuk,
            'belum' => $belum,
            'tokenGenerated' => $tokenGenerated,
            'tokenGeneratedBelum' => $total - $tokenGenerated,
        ];

        return view('saksi.pages.suara_masuk.index', $data);
    }
}
