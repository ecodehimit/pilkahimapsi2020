<?php

namespace App\Http\Controllers\Saksi;

use App\Http\Controllers\Controller;
use App\Models\HasilPemilu;
use App\Models\Candidates;
use App\Models\LogsWeb;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('saksi.pages.dashboard.index');
    }

    public function update()
    {
        $data = [
            'ketua_pelaksana' => LogsWeb::with('users')->where('users_id', '3')->first(),
            'panwas1' => LogsWeb::with('users')->where('users_id', '9')->first(),
            'panwas2' => LogsWeb::with('users')->where('users_id', '10')->first(),
            'saksi_paslon' => LogsWeb::with('users')->where('users_id', '12')->first(),
        ];
        return view('saksi.pages.dashboard.saksi', $data);
    }

    public function show()
    {
        $calon1 = Candidates::where('no_urut', 1)->first();
        $calon2 = Candidates::where('no_urut', 0)->first();
        $totalcalon1 = HasilPemilu::where('candidates_id', $calon1->id)->get();
        $totalcalon2 = HasilPemilu::where('candidates_id', $calon2->id)->get();
        $data = [
            'total1' => count($totalcalon1),
            'total2' => count($totalcalon2),
            'calon1' => $calon1,
            'calon2' => $calon2,
        ];
        
        return view('saksi.pages.dashboard.pengumuman', $data);
    }
}
