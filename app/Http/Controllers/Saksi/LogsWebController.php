<?php

namespace App\Http\Controllers\Saksi;

use App\Http\Controllers\Controller;
use App\Models\LogsWeb;

class LogsWebController extends Controller
{
    public function index()
    {
        $data['data'] = LogsWeb::with('users')->orderBy('id', 'DESC')->get();

        return view('saksi.pages.logs_web.index', $data);
    }
}
