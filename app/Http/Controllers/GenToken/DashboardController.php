<?php

namespace App\Http\Controllers\GenToken;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('gen-token.pages.dashboard.index');
    }
}
