<?php

namespace App\Http\Controllers\GenToken;

use App\Http\Controllers\Controller;
use App\Models\Mahasiswa;
use App\Models\Token;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Datatables;

class TokenController extends Controller
{
    public function index(Request $request)
    {
        $data['data'] = Token::orderBy('id', 'DESC')->get();
        $data['mahasiswa'] = Mahasiswa::where('status_pilih', '=', 'Belum')->get();
        if ($request->ajax()) {
            $token = Token::orderBy('id', 'DESC')->get();

            return DataTables::of($token)
                ->addColumn('durasi', function ($data) {
                    return '<p class="timer" data-create="'.$data->updated_at.'" data-id="'.$data->id.'" data-status="'.$data->status_token.'"></p>';
                })
                ->addColumn('action', function ($data) {
                    return '<a href="'.url('gen-token/regenerate/token/'.$data->id).'" onclick="return confirm('.'`Are you sure?`'.')" class="btn btn-info"><i class="fa fa-pencil"></i> Regenerate</a>';
                })
                ->rawColumns(['durasi', 'action'])
                ->make(true)
            ;
        }

        return view('gen-token.pages.token.index', $data);
    }

    public function store(Request $request)
    {
        $req = $request->all();

        $random = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $req['token'] = substr(str_shuffle(str_repeat($random, 5)), 0, 5);

        $req['status_token'] = 0;

        $nrp = $req['nrp'];
        $count = Token::where('nrp', $nrp)->count();
        $cek_mahasiswa = Mahasiswa::where([['nrp', '=', $nrp], ['status_pilih', '=', 'Sudah']])->count();

        if ($count > 0 || $cek_mahasiswa > 0) {
            if ($cek_mahasiswa > 0) {
                return redirect()->back()->with('danger', 'Kamu sudah menggunakan hak pilihmu');
            }

            return redirect()->back()->with('danger', 'Kamu sudah punya token');
        }
        $result = Token::create($req);

        return redirect()->back()->with('success', 'Token berhasil dibuat');
    }

    public function show($id)
    {
        $data['data'] = DB::table('q_token')->where('id', $id)->first();

        return view('gen-token.pages.token.show', $data);
    }

    public function edit($id)
    {
        $data['data'] = Token::find($id);

        return view('gen-token.pages.token.edit', $data);
    }

    public function update($id, Request $request)
    {
        $id = $request['id'];
        Token::where('id', $id)->update(['status_token' => 1]);
    }

    public function destroy($id)
    {
        $result = Token::find($id);
        $result->delete();

        return redirect()->back()->with('danger', 'delete data');
    }

    public function regenerate(Request $request, $id)
    {
        $req = $request->all();
        $random = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $new_token = substr(str_shuffle(str_repeat($random, 5)), 0, 5);

        $status_token = 0;

        $token = Token::where('id', $id)->first();

        $mahasiswa = Mahasiswa::where('nrp', $token->nrp)->first();

        if ('Sudah' == $mahasiswa->status_pilih) {
            return redirect()->back()->with('danger', 'Kamu sudah menggunakan hak pilihmu');
        }
        if ($token->count < 1) {
            $update = Token::where('id', $id)->update(['count' => $token->count + 1, 'token' => $new_token, 'status_token' => $status_token]);

            return redirect()->back()->with('success', 'Berhasil melakukan regenerate token');
        }

        return redirect()->back()->with('danger', 'Kamu sudah 2 kali generate token');
    }
}
