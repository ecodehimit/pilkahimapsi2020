<?php

namespace App\Http\Controllers\GenToken;

use App\Http\Controllers\Controller;
use App\Models\Mahasiswa;

class SuaraController extends Controller
{
    public function index()
    {
        $total = Mahasiswa::count();
        $suaraMasuk = Mahasiswa::where('status_pilih', '!=', 'Belum')->count();
        $suaraBelum = $total - $suaraMasuk;
        $masuk = ($suaraMasuk / $total) * 100;
        $belum = (($total - $suaraMasuk) / $total) * 100;

        $data = [
            'masuk' => $masuk,
            'belum' => $belum,
        ];

        return view('panitia.pages.suara_masuk.index', $data);
    }
}
