<?php

namespace App\Http\Controllers\Panitia;

use App\Http\Controllers\Controller;
use App\Models\HasilPemilu;
use App\Models\Mahasiswa;

class SuaraController extends Controller
{
    public function index()
    {
        $total = Mahasiswa::count();
        //        $tokenGenerated = Token::query()->count();
        //        $suaraMasuk = MahasiswaInformatika::where('status_pilih', '!=', 'Belum')->count();
        //        $suaraBelum = $total - $suaraMasuk;
        //        $masuk = ($suaraMasuk/$total) * 100;
        //        $hasToken = ($tokenGenerated/$total) * 100;
        //        $belum = (($total - $suaraMasuk) / $total ) *100;

        $total = Mahasiswa::count();
        $tokenGenerated = HasilPemilu::query()->count();
        $suaraBelum = $total - $tokenGenerated;
        $masuk = ($tokenGenerated / $total) * 100;
        $belum = (($total - $tokenGenerated) / $total) * 100;

        $data = [
            'masuk' => $masuk,
            'belum' => $belum,
            'tokenGenerated' => $tokenGenerated,
            'tokenGeneratedBelum' => $total - $tokenGenerated,
            //            'has_token' => $hasToken
        ];

        return view('panitia.pages.suara_masuk.index', $data);
    }
}
