<?php

namespace App\Http\Controllers\Panitia;

use App\Http\Controllers\Controller;
use App\Models\Candidates;
use App\Models\Mahasiswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class CandidatesController extends Controller
{
    public function index()
    {
        $data['data'] = Candidates::orderBy('id', 'ASC')->get();

        $data['mahasiswa'] = Mahasiswa::get();

        return view('panitia.pages.candidates.index', $data);
    }

    public function create()
    {
        $data['mahasiswa'] = Mahasiswa::get();

        return view('panitia.pages.candidates.create', $data);
    }

    public function store(Request $request)
    {
        $req = $request->all();

        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $destinationPath = 'img/'; // upload path
                $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111, 99999).'.'.$extension; // renaming image
                $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
                Image::make($destinationPath.$fileName)->resize(500, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save($destinationPath.$fileName);
                $req['photo'] = $fileName;
                unset($req['image']);
            }
        }

        $result = Candidates::create($req);

        return redirect('panitia/candidates')->withInput()->with('message', [
            'title' => 'Yay!',
            'type' => 'success',
            'msg' => 'Saved Success.',
        ]);
    }

    public function edit($id)
    {
        $data['data'] = Candidates::find($id);

        $data['mahasiswa'] = Mahasiswa::get();

        return view('panitia.pages.candidates.edit', $data);
    }

    public function update($id, Request $request)
    {
        //        dd($request->file('image'));

        $req = $request->except('_method', '_token', 'submit');

        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $destinationPath = 'img/'; // upload path
                $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111, 99999).'.'.$extension; // renaming image
                $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
                Image::make($destinationPath.$fileName)->resize(500, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save($destinationPath.$fileName);
                $req['photo'] = $fileName;
                unset($req['image']);

                $result = Candidates::find($id);
                if (!empty($result->photo)) {
                    File::delete('img/'.$result->photo);
                }
            } else {
                unset($req['photo']);
            }
        } else {
            unset($req['photo']);
        }

        $result = Candidates::where('id', $id)->update($req);

        return redirect('panitia/candidates')->withInput()->with('message', [
            'title' => 'Yay!',
            'type' => 'success',
            'msg' => 'Saved Success.',
        ]);
    }

    public function destroy($id)
    {
        $result = Candidates::find($id);
        $result->delete();

        return redirect('panitia/candidates')->withInput()->with('message', [
            'title' => 'Yay!',
            'type' => 'success',
            'msg' => 'Deleted data  .',
        ]);
    }

    public function show(Request $req)
    {
        $nama = $req['nama'];
        $data = Mahasiswa::where('nama', '!=', $nama)->get();
        echo '<option selected disabled>Pilih Calon Wakahima</option>';
        foreach ($data as $wakil) {
            echo '<option>'.$wakil->nama.'</option>';
        }
    }
}
