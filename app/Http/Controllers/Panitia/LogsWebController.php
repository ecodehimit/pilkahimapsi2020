<?php

namespace App\Http\Controllers\Panitia;

use App\Http\Controllers\Controller;
use App\Models\LogsWeb;

class LogsWebController extends Controller
{
    public function index()
    {
        $data['data'] = LogsWeb::with('users')->orderBy('id', 'DESC')->get();

        return view('panitia.pages.logs_web.index', $data);
    }
}
