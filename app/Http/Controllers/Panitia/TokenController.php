<?php

namespace App\Http\Controllers\Panitia;

use App\Http\Controllers\Controller;
use App\Models\Mahasiswa;
use App\Models\Token;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TokenController extends Controller
{
    public function index()
    {
        $data['data'] = Token::orderBy('id', 'DESC')->get();
        $data['mahasiswa'] = Mahasiswa::get();

        return view('panitia.pages.token.index', $data);
    }

    public function store(Request $request)
    {
        $req = $request->all();

        $random = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $req['token'] = substr(str_shuffle(str_repeat($random, 5)), 0, 5);

        $req['status_token'] = 0;

        $nrp = $req['nrp'];
        $count = Token::where('nrp', $nrp)->count();
        $cek_mahasiswa = Mahasiswa::where([['nrp', '=', $nrp], ['status_pilih', '=', 'Sudah']])->count();

        if ($count > 0 || $cek_mahasiswa > 0) {
            if ($cek_mahasiswa > 0) {
                return redirect()->back()->with('danger', 'Kamu sudah menggunakan hak memilihmu');
            }

            return redirect()->back()->with('warning', 'NRP has been Registered');
        }
        $result = Token::query()->create($req);

        return redirect('panitia/token/show/'.$result->id)->with('success', 'saved success');
    }

    public function show($id)
    {
        $data['data'] = DB::table('q_token')->where('id', $id)->first();

        return view('panitia.pages.token.show', $data);
    }

    public function edit($id)
    {
        $data['data'] = Token::find($id);

        return view('panitia.pages.token.edit', $data);
    }

    public function update($id, Request $request)
    {
        $id = $request['id'];
        Token::where('id', $id)->update(['status_token' => 1]);
    }

    public function destroy($id)
    {
        $result = Token::find($id);
        $result->delete();

        return redirect('panitia/token')->withInput()->with('message', [
            'title' => 'Yay!',
            'type' => 'success',
            'msg' => 'Deleted data.',
        ]);
    }
}
