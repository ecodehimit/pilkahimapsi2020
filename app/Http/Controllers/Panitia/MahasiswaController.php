<?php

namespace App\Http\Controllers\Panitia;

use App\Http\Controllers\Controller;
use App\Models\Kelas;
use App\Models\Mahasiswa;
use Illuminate\Http\Request;
use Yajra\DataTables\Datatables;
use App\Imports\MahasiswaImport;
use Maatwebsite\Excel\Facades\Excel;

class MahasiswaController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Mahasiswa::with('kelas')->orderBy('id', 'DESC')->get();
            //            $data = Mahasiswa::latest()->get();
            return DataTables::of($data)
                ->addColumn('kelas', function ($data) {
                $p = $data->kelas->tahun; //.$data->kelas->jenjang.' '.$data->kelas->kelas;
                return $p;
                })
                ->addColumn('action', function ($data) {
                $button = '<a href="mahasiswa/' . $data->id . '/edit' . '" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                $button .= '<a href="' . route('mahasiswadestroypanitia', $data->id) . '" class="btn btn-danger" onclick="return confirm(' . 'Are you sure?' . ')"><i class="fa fa-trash"></i> Delete</a>';

                return $button;
                })
                ->rawColumns(['kelas', 'action'])
                ->make(true);
        }
    }

    public function create()
    {
        $data['kelas'] = Kelas::get();

        return view('panitia.pages.mahasiswa.create', $data);
    }

    public function store(Request $request)
    {
        $req = $request->all();

        $result = Mahasiswa::create($req);

        return redirect('panitia/mahasiswa')->with('success', 'Berhasil Di tambah');
    }

    public function edit($id)
    {
        $data['kelas'] = Kelas::get();
        $data['data'] = Mahasiswa::find($id);

        return view('panitia.pages.mahasiswa.edit', $data);
    }

    public function update($id, Request $request)
    {
        $req = $request->except('_method', '_token', 'submit');

        $result = Mahasiswa::where('id', $id)->update($req);

        return redirect('panitia/mahasiswa')->with('success', 'Berhasil Di update');
    }

    public function destroy($id)
    {
        $result = Mahasiswa::find($id);
        $result->delete();
        return redirect()->back()->with('danger', 'Berhasil Di delete');
    }

    public function toImport()
    {
        return view('backend.pages.mahasiswa.import');
    }

    public function import(Request $request)
    {
        $request->validate([
            'excel' => 'required|mimes:xlsx'
        ]);

        Excel::import(new MahasiswaImport, $request->excel);
        return redirect('backend/mahasiswas')
            ->with('success', 'Data imported successfully.');
    }
}
