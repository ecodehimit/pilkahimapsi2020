<?php

namespace App\Http\Controllers;

use App\Models\Candidates;
use App\Models\Devices;
use App\Models\HasilPemilu;
use App\Models\Mahasiswa;
use App\Models\Token;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LoginsController extends Controller
{
    public function login(Request $request)
    {
        $req = $request->all();
        $nrp = $req['nrp'];
        $token = $req['token'];
        //        $data = User::where('id',1)->first();
        //        $kandidat = Candidates::all();
        //        return view('voting.vote', compact(['data','kandidat']));
        //        $session = $request->

        if (1 == Token::where('nrp', $nrp)->where('token', $token)->count()) {
            $ip = $_SERVER['REMOTE_ADDR'];
            $data = Token::where('nrp', $nrp)->first();
            $candidates = Candidates::where('no_urut', -1)->first();
            $mahasiswa = Mahasiswa::where('nrp', $nrp)->first();
            if (Carbon::now()->diffInMinutes($data->updated_at) >= 5) {
                return redirect('/vote')->with('danger', 'token kadaluarsaaa');
            }

            $stat_token = $data->status_token;

            if (0 == $stat_token) {
                $mahasiswa->status_token = 1;
                $mahasiswa->save();
                Token::where('nrp', $nrp)->update(['status_token' => 1]);
                $kandidat1 = Candidates::where('id','=',1)->first();
                $kandidat2 = Candidates::where('id','=',2)->first();

//                dd($kandidat1);

                return view('voting.vote', compact(['data', 'kandidat1','kandidat2', 'mahasiswa']));
            }
            if (1 == $stat_token) {
                if (1 == $data->count) {
                    //                        Mahasiswa::where('nrp', $nrp)->update(['status_pilih' => 'Sudah']);
                    $object = [
                        'nrp' => $nrp,
                        'candidates_id' => $candidates->id,
                    ];
                    HasilPemilu::create($object);

                    Mahasiswa::where('nrp', $nrp)->update([
                        'status_pilih' => 'Sudah',
                        'voted_at' => now(),
                    ]);

                    Devices::where('ip_address', $ip)->update([
                        'active_client' => 'Tidak Aktif',
                    ]);
                }
                Token::where('nrp', $nrp)->update(['status_token' => 1]);

                return redirect('/vote')->with('danger', 'token kadaluarsa');
            }
        } else {
            return redirect('/vote')->with('danger', 'NRP atau TOKEN salah')->withInput();
        }
    }

    public function pilih(Request $request)
    {
        $req = $request->except('nrp', 'token');
        $req2 = $request->only('nrp', 'token');

        $nrp = $req2['nrp'];
        $token = $req2['token'];
        $no_urut = $req['no_urut'];
//        dd($no_urut);
        $ip = 'web';
        $mahasiswa = Mahasiswa::where('nrp', $nrp)->first();
        if ('Sudah' == $mahasiswa->status_pilih) {
            return redirect('/vote')->with('danger', 'gagal memilih');
        }

        if (1 == Token::where('nrp', $nrp)->where('token', $token)->count()) {
            $data = Token::where('nrp', $nrp)->first();
            $stat_token = $data->status_token;
            if (1 == $stat_token) {
                $candidates = Candidates::where('no_urut', $no_urut)->first();
                // dd($candidates);
                $object = [
                    'nrp' => $nrp,
                    'candidates_id' => $candidates->id,
                ];
                HasilPemilu::create($object);

                Mahasiswa::where('nrp', $nrp)->update([
                    'status_pilih' => 'Sudah',
                    'voted_at' => now(),
                ]);

                Devices::where('ip_address', $ip)->update([
                    'active_client' => 'Tidak Aktif',
                ]);
                return redirect('/berhasil')->with('success', 'berhasil memilih');
            }

            return redirect('/vote')->with('danger', 'gagal memilih');
        }

        return redirect('/vote')->with('warning', 'gagal memilih');
    }


    public function berhasil()
    {
        if (Session::has('success')) {
            return view('voting.hasilmemilih');
        }

        return redirect('/vote');
    }
}
