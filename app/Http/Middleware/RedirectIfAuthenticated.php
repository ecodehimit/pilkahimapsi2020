<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                if ($user = auth()->user()){
                    if ($user->role_web == 'admin') {
                        return redirect(route('backend.dashboard.index'));
                    }
                    if ($user->role_web == 'panitia') {
                        return redirect(route('panitia.dashboard.index'));
                    }
                    if ($user->role_web == 'saksi') {
                        return redirect(route('saksi.dashboard.index'));
                    }
                    if ($user->role_web == 'gen-token') {
                        return redirect(route('gen-token.dashboard.index'));
                    }
                }
                return redirect('/');
            }
        }

        return $next($request);
    }
}
