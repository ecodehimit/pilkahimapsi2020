<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoleWeb
{
    /**
     * Handle an incoming request.
     *
     * @param null|mixed $role
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $role = null)
    {
        if (Auth::check()) {
            $role_db = auth()->user()->role_web;

            if ($role_db != $role && 'admin' != $role_db) {
                return redirect()->back();
            }
        }

        return $next($request);
    }
}
