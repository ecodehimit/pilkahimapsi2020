<?php

namespace App\Exports;

use Carbon\Carbon;
use App\Models\Mahasiswa;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MahasiswaExport extends DefaultValueBinder implements WithCustomValueBinder, FromQuery, WithMapping, WithHeadings, ShouldAutoSize
{
    public function headings(): array
    {
        return [
            'tahun',
            'NIM',
            'Nama',
            'Status Pilih',
            'Waktu Pilih',
        ];
    }

    public function map($mahasiswa): array
    {
        if($mahasiswa->voted_at) {
            $mahasiswa->voted_at = Carbon::parse($mahasiswa->voted_at)->toDateTimeString();
        }

        return [
            $mahasiswa->kelas->tahun,
            $mahasiswa->nrp,
            $mahasiswa->nama,
            $mahasiswa->status_pilih,
            $mahasiswa->voted_at,
        ];
    }

    public function query()
    {
        return Mahasiswa::query();
    }

    public function bindValue(Cell $cell, $value)
    {

            $cell->setValueExplicit($value, DataType::TYPE_STRING);
            return true;
    }
}
