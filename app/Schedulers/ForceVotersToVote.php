<?php

namespace App\Schedulers;

use App\Models\Candidates;
use App\Models\HasilPemilu;
use App\Models\Mahasiswa;
use App\Models\Token;
use Carbon\Carbon;

class ForceVotersToVote
{
    public function __invoke()
    {
        /**
         * Tentukan batas waktu voting (menit)
         */
        $batas_waktu = 10;
        $sekarang = Carbon::now()->subMinutes($batas_waktu);

        /**
         * Ambil kandidat abstain / kosong.
         */
        $candidates = Candidates::where('no_urut', 0)->first();

        /*
         * Ambil mahasiswa dengan kriteria berikut
         * 1. Status token 1
         * 2. Status pilih Belum
         * 3. Waktu login sudah melewati batas yang telah ditentukan
         */
        $mahasiswa = Mahasiswa::where('status_token', 1)
            ->where('status_pilih', 'Belum')
            ->where('updated_at', '<', $sekarang)
            ->get()
        ;

        $nrp = $mahasiswa->map(function($mhs){
            return $mhs->nrp;
        });

        /**
         * Update status pilih menjadi sudah memilih
         */
        Mahasiswa::whereIn('nrp', $nrp)->update([
            'status_pilih' => 'Sudah',
        ]);

        /**
         * Masukkan data pilihan kedalam hasil pemilu
         */
        $hasil_pemilu = [];
        foreach ($nrp as $data) {
            $hasil_pemilu[] = [
                'nrp' => $data,
                'candidates_id' => $candidates->id,
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }
        HasilPemilu::insert($hasil_pemilu);

        /**
         * Ubah token agar tidak bisa digunakan
         */
        Token::whereIn('nrp', $nrp)->update(['status_token' => 1]);
    }
}
